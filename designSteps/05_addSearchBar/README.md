# Search bar component

The search bar starts off as a search icon ('closed state').


![Search-open](./readme-assets/search_closed.png)

On roll-over, the search icon will transition to a search input ('open state').

![Search-open](./readme-assets/search_open.png)

If the user enters nothing, the search input will transition back to the closed state on blur.

If the user enters a search term, the search input will stay in the open state, and if the user deletes the search term (either by direct typing of clicking the 'x' icon), the search component will go back to the closed state.

![Search-open](./readme-assets/search_populated.png)

So, 
- The search appears as a small icon when not in use.
- It opens and becomes interactive only when interacted with.
- The user knows if a search filter is being applied simply because the search input remains open.

## Notes

1. The html file is the index from 01-appLayout, modified to add search to the header. 