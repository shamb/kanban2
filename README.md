# Introduction

This repo was used as part of a series of tech-talks at Covea Insurance, Halifax, UK. It covers React using function components, TypeScript and testing-library/react. It is designed as a 'next steps' guide for a ReactJS beginner who has got past their first basic 'to-do' application.

This readme file is a copy of the one found in the /designSteps/01_appLayout folder and covers initial application layout. Further readme files can be found in the designSteps sub-folders to guide you though other design decisions made during the application.

All code has unit tests in the __tests__ files. The test coverage report can be seen by viewing the file coverage/lcov-report/index.html in a browser.

A fuller, end to end readme will be written if enough people request it.


# Layout

## Starting your application

Assuming you have a basic functional spec (a overall purpose and at least basic user requirements for your application), your next step is to write HTML/CSS mockups. 

These are either created by 
- the UX designer, or 
- the UX designer creates Photoshop mockups and you create the HTML/CSS from it. 

If the UX designer creates the HTML/CSS mockups, _you need to ensure they are compatible with JavaScript/React, rewriting them as neccessary before you bring them into React._ We will address this later (below).

### Why you should create the HTML/CSS before your React components
Creating your HTML/CSS mockups rather than diving straight into create-react-app has a number of advantages;
* Structural
  * You remain independent to framework choices until you have a far better idea of what you need to build. This enables you to make better framework choices; Angular, Vue, Svelte or no framework may be better choices than React.
  * Assuming your HTML is well formed (i.e. properly nested) means the choice of what your individual components will contain (and the data-flow) becomes obvious just by looking at the markup.
  * If you can't create basic look and feel with HTML/CSS, you are not ready to jump into a front end framework yet! Doing the layout in HTML/CSS first is a good way of testing your early assumptions.
  * You can fix layout problems before introducing framework and third party library code. If you skip this step and go straight into your framework, you often get false positives of the _its a framework render problem_ kind, and these can take a _long_ time to resolve.
* Workflow
  * You have something to sign off/agree with the client/project team before starting your coding. This puts one of your major project milestones (and a big sanity check!) _before_ your main coding effort, and can make the project planning easier. It also ensures your technical uncertainties are addressed up front.


### Real world Example 
The following **Photoshop mockup** was provided to me by UX in a previous contract. It specifies a new page in an existing web application;

![mockup1](./readme-assets/GPIO-01.png)

Here's screenshots of the first few stages I used to implement this;

![grid implementation 1](./readme-assets/routingMatrix01.png)

*Step 1: Blocked out the entire page as a single placeholder div within the working application.*

It is a good idea to always set placeholders to a particular colour. I use grey (or yellow if grey conflicts with 'disabled' styling).

![grid implementation 2](./readme-assets/routingMatrix03.png)
*Step 2: Break down the div into separate sections for the logically separate page sections*

There are 5 sections;
* The Transmitter and Receiver filters (labeled as _transmitters_ and _Receivers in the image_).
* The two tree views that form the axes of the table (_transmitters go here_ and _receivers go here_). Note that one is rotated by 90 degrees, so as part of the layout, I had to write CSS that ensured that the tree view appeared rotated but the contents did not 'know or care'.
* The grid (_grid goes here_).

Each of these sections are implemented as component stubs (consisting of one styled but empty div container each).

![grid implementation 2](./readme-assets/routingMatrix03b.png)

Each stub component is then fleshed out as the page sections are developed. Above we see the first component section complete; the filter component and its dropdown as static markup in its 'full' (open) state.

![grid implementation 2](./readme-assets/routingMatrix04.png)

*Completed page markup*

The two trees along the axes are the same component (one is just rotated through 90 degrees). The grid itself is just a nested loop (an inner loop to create a single row of grid cells, and an outer loop to create multiple rows). There are 4 main components in all (a tree, tree-cell, grid-cell and grid-axis-filter).

The next steps from here were to;
- Get approval from the product owner and UX designer. 
- Implement the JS.

Note that the final working design looks _exactly_ like the mockup. **From experience, this _never_ happens unless HTML/CSS mockups are signed off as an intermediate step.** Without the intermediate step, there is no agile iteration between the JS and UX team to define workable layouts. 


## Example project

Refer to index.html in designSteps/01_appLayout or... 
<details><summary>click here to see it.</summary>
<p>

```html
<!DOCTYPE HTML>
<html>
  
  <head>
    <style>

      html, body {
        margin: 0;
        padding: 0;
        height: 100%;
        width: 100%;
        font-family: Arial, Helvetica, sans-serif;
        color: #606060;
        box-sizing: border-box;
        overflow: hidden;
      }

      /** app is the main container class.
        * It takes up the height of the browser, and takes up 
        * 100% browser width by default (as it is a block).
        * It also sets the application background colour. 
        */
      .app {
        height:100%;
        background-color: #f2f2f5;
      }

      /** board is the swimlanes container. It is a flexbox column 
        * and this forces the header and swimlane container to
        * stack vertically.
        * NB - note that board has max-width set to fit-content 
        * so that .header-bar does not extend to the full browser 
        * width.
        */ 
      .board {
        display:flex;
        flex-direction: column;
        height: 100%;
        margin: 0 4px 0 4px;
        max-width: fit-content;       /* chrome */
        max-width: -moz-max-content;  /* firefox */
        max-width: intrinsic;         /* Safari? */
      }

      /** .header-bar is the application header.
        * It floats the board title to the left and and menu buttons 
        * to the right, all done via a flexbox with 
        * justify-content: space-between.
        */
      .header-bar {
        display: flex;
        flex-direction: row;
        justify-content: space-between;
        padding-top: 2px;
        padding-bottom: 2px;
        padding-left: 0;
        padding-right: 0;
      }

      /** container for a row of buttons that lie immediately 
        * adjacent to each other. Rounds the leftmost and rightmost
        * edges.
        */

      .button-group {
        display:flex;
        flex-shrink: 0;
        flex-direction: row;
      }

      .button-group .btn {
        border: 1px solid #dedede;
        border-right: none;
        min-width: 60px;
        min-height: 2.5em;
        padding-left: 1.5em;
        padding-right: 1.5em;
        color: #606060;
        background-color: #ffffff; 
      }
      .button-group .btn:hover {
        background-color: #c7dbe4;
      }
      .button-group .btn:focus {
        outline:0;
      }
      
      .button-group .btn:first-child {
        border-right: none; 
        border-radius: 12px 0 0 12px;
      }
      .button-group .btn:last-child {
        border-right: 1px solid #dedede; 
        border-radius: 0 12px 12px 0;
      }

      .button-group .icon {
        background-repeat: no-repeat;
        background-position: 1em 45%;
        background-size: 1em;
        padding-left:2.5em;
      }
      .button-group .edit {
        background-image: url('./assets/svg/md-create.svg');
      }

      .button-group .reports {
        background-image: url('./assets/svg/md-trending-up.svg');
      }

      .header-bar .button-group {
        margin-top: auto;
        margin-bottom: auto;
      }

      /** swimlanes is the class for the container that  contains 
        * the individual swimlane columns. This container has the 
        * vertical scrollbar via overflow: auto.
        * swimlane-container is the individual board columns.
        * They are a simple flexbox column with minimum width of 
        * 300px. The hover effect (where the columns appear to 
        * get a little larger on rollover) is done by setting
        * the border transparent on rollover.
        */
      .swimlanes {
        display: flex;
        height: 100%;
        flex-direction: row; /* default */
        flex-wrap: nowrap;
        overflow:auto;
        padding: 0;
        margin: 0;
        scrollbar-width: thin; /* for Firefox */
      }
      .swimlane-container {
        display: flex;
        flex-direction: column;
        min-width: 300px;
        max-width: 300px;
        min-height: 300px;
        border: 2px solid #dedede;
        border-radius: 10px;
        margin: 5px 10px 5px 10px;
        background-color: #fdfcff;
        transition: all .3s;
        overflow: hidden;
        padding-top: 2px;
      }
      .swimlane-container:first-child {
        margin-left: 0;
      }
      .swimlane-container:last-child {
        margin-right: 0;
      }
      .swimlane-container:hover {
        background-color: #ffffff;
        box-shadow: 0px 2px 8px rgba(0, 0, 0, 0.2);
        border-color: transparent;
        padding-top: 0;
      }

      .swimlane-head {
        height: 48px;
        flex-shrink: 0;
        position: relative;
        line-height: 48px;
        border-bottom: 1px solid #e9e9e9;
        vertical-align: middle;
        overflow: hidden;
        padding: 0 24px;
      }
      .swimlane-body {
        position: relative;
        display: flex;
        flex-direction: column;
        padding: 0 24px;
        overflow-x: hidden;
        overflow-y: auto;
        flex-basis: 100%;
        margin-top: 2px;
        margin-bottom: 2px;
      }

      .story-card {
        position: relative;
        flex-shrink: 0;
        z-index: 1;
        background-color: #fffff5;
        padding: 10px 10px 10px 10px;
        margin: 0 0 10px 0;
        overflow: hidden;
        border: 1px solid #d5d5cf;
        border-left: 8px solid #60cc60;
        border-radius: 0px;
        box-shadow: 0px 1px 4px rgba(0, 0, 0, 0.1);
        transition: opacity 0.5s;
      }

      /** scrollbar styling.
        * Note this only works for Chrome. For other browsers, we 
        * can only use scrollbar-width: thin (see .swimlanes). 
        */
      ::-webkit-scrollbar {
        width: 10px;
	      height:10px;
      }
      ::-webkit-scrollbar-track {
        -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
	      box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
        border-radius: 10px;
      }
      ::-webkit-scrollbar-thumb {
        border-radius: 10px;
	      background:#aaa;
      }

      .ellipses-text {
        white-space: nowrap;
        overflow: hidden;
        text-overflow: ellipsis;

      }

    </style>
    
  </head>
  
  <body>
    
    <div class="app">

      <div class="board">
      
        <header class="header-bar">
          <h2 class="ellipses-text">Board Title</h2>
          <div class="button-group">
            <button type="button" class="btn icon reports">Reports</button>
            <button type="button" class="btn icon edit">Edit Board</button>
            <button type="button" class="btn">Standard button</button>
            <button type="button" class="btn">Standard button</button>
          </div>
        </header>

        <div class="swimlanes">
          
          <div class="swimlane-container">
            <div class="swimlane-head ellipses-text">
              Backlog (A very long title that will cause ellipses to occur)
            </div>
            <div class="swimlane-body">
              <div class="story-card" draggable="true">
                <h4>This is a story card title</h4>
                This is a story card summary, and will include things like 
                story-points, originator and assigned-developer. 
              </div>
              <div class="story-card" draggable="true">
                <h4>This is a story card title</h4>
                This is a story card summary, and will include things like 
                story-points, originator and assigned-developer. 
              </div>
              <div class="story-card" draggable="true">
                <h4>This is a story card title</h4>
                This is a story card summary, and will include things like 
                story-points, originator and assigned-developer. 
              </div>
              <div class="story-card" draggable="true">
                <h4>This is a story card title</h4>
                This is a story card summary, and will include things like 
                story-points, originator and assigned-developer. 
              </div>
            </div>
          </div>
          
          <div class="swimlane-container">
            <div class="swimlane-head ellipses-text">
              Ready
            </div>
            <div class="swimlane-body"></div>
          </div>
          
          <div class="swimlane-container">
              <div class="swimlane-head ellipses-text">
                In Progress
              </div>
            <div class="swimlane-body"></div>
          </div>
          
          <div class="swimlane-container">
              <div class="swimlane-head ellipses-text">
                Test
              </div>
            <div class="swimlane-body"></div>
          </div>

          <div class="swimlane-container">
              <div class="swimlane-head ellipses-text">
                Done
              </div>
            <div class="swimlane-body"></div>
          </div>
      
        </div>
      
      </div>

    </div>
    
    <script>

        
    </script>

  </body>

</html>
```

</p>
</details>



We will be creating a configurable progress board, similar to the first application most React developers try. 

![Kanban](./readme-assets/kanban01.png)

*The kanban*

You can add, rename and remove columns by going into an edit mode. 

![Kanban edit mode](./readme-assets/kanban02.png)

*Kanban edit mode*

You can of course also move story items via drag-drop.

![Kanban drag-drop](./readme-assets/kanban03.png)

*Kanban drag-drop*

By removing all but two columns, we can configure the board as a simple 'ToDo' board, but of course still have drag-drop functionality.

![TODO](./readme-assets/kanban04.png)

*Turning the 3 column Kanban into a simple TODO*

By adding scrum style columns (backlog, ready, in progress, etc), we can reconfigure the board to show a full Agile planner. This is the one Ocean team use;

![Ocean Sprint board](./readme-assets/kanban05.png)

*Configuring the Kanban into the Ocean team sprint board*

Better still, we can change from a simple ToDo to a three column Kanban, to a multi column Agile board _while a project is in progress_.

## Creating the HTML/CSS

Our layout looks something like this;

![Block level layout](./readme-assets/layout01.png)

**Block level layout**

A web application layout differs from a website layout because the former has to be much stricter in its nesting. A website design is nested for visual reasons, but a web application has to be **logically nested**.
All this means in practice is that we have to do is make sure we follow a logical top-down flow; 
- The **app** consists of a **board**.
- The board consists of a **header** and **swim-lanes**.
- The swimlanes can contain **stories**

**It is important to layout your app in terms of logically nested containers, because you will pick up this nesting later when you come to define your React components and the data flow between them.**

There are a number of alternative layouts that a web designer might make at this point that a web application developer should avoid/rewrite because they tend to break the logical nesting flow;

- Avoid any absolute containers that are not logically contained by something else. There are very good reasons to pick absolute positioning, usually for _performance_ (an absolute div does not redraw as part of the document flow) or because the absolute div is an overlay or 'sticky' (stays in the same position as the rest of the viewport scrolls) but don't pick absolute positioning simply because it is the easiest way to perfectly position something. There is often a better (more semantic) way of doing it that creates more obvious parent-child containers that make refactor into React components easier later.  
- Positioning via floats. Floats are neither very semantic (they were designed for text not containers) and can often be fragile. Use Flexbox instead. 
- Inline-blocks. You will often see web/UX developers chose `display:inline-block` so they can explicitly set height _and_ width, or so they can make a container shrink width to match its content ('shrink-wrap to its content'). Unfortunately, inline-block often breaks scrollbars _and_ you don't notice this until your lists and tables start to fill up with content (usually well into the project, requiring a costly re-write!). Instead use `max-width:value` (also set `min-width` to the same value if the container can be empty) to set a specific width. Use `max-width:fit-content` for shrink-wrap to content in Chrome. For firefox use `-moz-max-content` and `intristic` for Safari (can someone check this as I don't have a Mac). Note that just setting `width:value` often doesn't work (its a heuristic and although the browser will try to meet width, there are many reasons why it often doesn't!).
- fixed width layouts pretending to be dynamic. Web/UX designers will often submit HTML/CSS layouts that look to be dynamic, but are sized to fix the current content exactly, so are really fixed-width/fixed-height, and this causes rework when you get real data. So for example, you might get a trello board design with 4 swimlanes and everything looks sweet... until you add another swimlane! Check the layout by copy-pasting and deleting HTML in the Chrome **Elements** tab to simulate adding/deleting swimlanes to make sure the layout works with dynamic content (experience has shown the web application developer always has to add markup to make it work, and the earlier you do it, the better, because the later you do it, the more likely it is that you do it in your own time!).

**An easy rule of thumb is to always use flexbox, unless there is a good reason not to.** With flexbox, the layout rules are always on the parent container, which makes for an easy and consistent rule (and you don't have to remember historic float/display rules!).

### Rules of thumb for laying out a web application

The following specific rules of thumbs are all the CSS you need to know to layout most web applications to block level.

#### Floating and centering
If you want to **display one item to the left and another to the right**, use `display:flex; flex-direction:row` and `justify-content:space-between` on the parent container.

This rule is used to position the title and button bar in our header;

![Layout of the header](./readme-assets/layout02.png)


If you want to **display one item to the right**, place that item in a container with display:flex; flex-direction:row-reverse` on the parent container.

For examplle, if the header only had a button bar and we still wanted it on the right, we would use `flex-direction:row-reverse` on the parent to start the row at the right.

![Layout of the header 2](./readme-assets/layout03.png)

*Placing an element to the right without floating*

If we wanted to centre an element, we would use `display:flex; justify-content:center`. If we did this to our (now lonely) button bar, and set the container height to 200px, the button bar would now be central in its parent container;  


![centering an element](./readme-assets/layout04.png)

*Centering an element*

#### Rows and columns

If you want to **create a row of elements**, use `display:flex; flex-direction:row` and `flex-wrap:nowrap` to avoid creating multiple rows.

This is used to layout our swimlanes into a row.

![Creating a row](./readme-assets/layout05.png)

*Layout out a row*

Note that you can still use `margin` to control margin based spacing in flexbox. It is usual to want a margin between elements of your row, but a different margin between the elements and their parent. To do this, set the far left and far right margins to 0 using `first-child` and `last-child` css selectors;

![Row margins](./readme-assets/layout06.png)

![Row margins](./readme-assets/layout07.png)

If you want to **create a column of elements** do the same as for rows, but set `flex-direction:column`.

We do this to arrange the stories within each swimlane;

![Creating a column](./readme-assets/layout08.png)

#### Fit to content and scrollbars

Fitting to content is always a pain point with dynamic content, especially if the thing that changes is width. Height is much easier because the default HTML actions handle it well enough, but HTML only works well with width only if its a fixed value (usually pixels or percent).

To address width, you need to shrink wrap to content, whilst allowing scrollbars to take over of the content overflows the available width.

To shrink-wrap a container to its content width, use `max-width: fit-content` (or `-moz-max-content` (Firefox) or `intristic` (Safari)). To controll scrollbars, set overflow:none everywhere except the parent containers where you want scrollbars to appear.

Note that `display:inline-block` (which many web/UX designers use for their mockups) _appears_ to work until your content overflows, whereupon you will find that scrollbars don't appear (because `inline` doesn't work well with scrollbars and overflow. 
![board has display:block](./readme-assets/layout09.png)
*With display:inline-block, the horizontal scrollbar doesn't appear on overflow*

Using 'display:block` will work for scrollbars, but the parent container will now take up the full width, which gives you issues as shown; the header extends to the full width of the page rather than just to the width of the board.


![board has display:block](./readme-assets/layout10.png)
*With block, scrollbars can be made to work correctly, but because the board container now takes up the full width of the browser viewport, the header goes too far to the right*

With `max-width:fit-content`, the board container now shrinks down to fit the swimlanes, so the board container (and therefore the header, which is inside board) stop at the right point. 

![Scrollbars working correctly](./readme-assets/layout11.png)
The swimlanes and board scrollbars naturally appear when required, so our components will not have to care. Also note that the header is always correctly sized.

### Other considerations

The title text correctly falls back to ellipses rather than truncating because of class `ellipses-text`

```css
.ellipses-text {
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
}
```

You can see this if you create long titles or decrease the browser width;


![Ellises text on long titles](./readme-assets/layout12.png)


The stories are already draggable (because they have the html `draggable` attribute set in the markup). The drop is not of course handled (and the layout is not dynamic in any case), but look and feel can already be tested.


![Drag drop](./readme-assets/layout13.png)


In the talk we briefly looked at ingesting the HTML markup into React as a single component called app. Here's what that component looked like

<details><summary>click here to see the component.</summary>
<p>

```javascript

import React from 'react';
import './App.css';

function App() {
  return (
    <div className="App">

      <div className="board">

        {/* TODO: Refactor into 'Header' */}
        <header className="header-bar">
          <h2 className="unselectable ellipses-text">Board Title</h2>
          {/* TODO: Refactor into 'ButtonGroup'*/}
          <div className="button-group">
            <button type="button" className="btn icon reports">Reports</button>
            <button type="button" className="btn icon edit">Edit Board</button>
            <button type="button" className="btn">Standard button</button>
            <button type="button" className="btn">Standard button</button>
          </div>
        </header>

        {/* TODO: Refactor into 'SwimLanes'.
            within it create a repeater loop that 
            generates multiple 'Swimlane' */}
        <div className="swimlanes">
          
          <div className="swimlane-container">
            <div className="swimlane-head unselectable ellipses-text">
              Backlog (A very long title that will cause ellipses to occur)
            </div>
            
            <div className="swimlane-body">
              <div className="story-card" draggable="true">
                <h4>This is a story card title</h4>
                This is a story card summary, and will include things like 
                story-points, originator and assigned-developer. 
              </div>

              {/* TODO: Within each Swimlane, create a repeater loop that generates the
                  cards. */}
              <div className="story-card" draggable="true">
                <h4>This is a story card title</h4>
                This is a story card summary, and will include things like 
                story-points, originator and assigned-developer. 
              </div>
              <div className="story-card" draggable="true">
                <h4>This is a story card title</h4>
                This is a story card summary, and will include things like 
                story-points, originator and assigned-developer. 
              </div>
              <div className="story-card" draggable="true">
                <h4>This is a story card title</h4>
                This is a story card summary, and will include things like 
                story-points, originator and assigned-developer. 
              </div>
            </div>
          </div>
          
          <div className="swimlane-container">
            <div className="swimlane-head unselectable ellipses-text">
              Ready
            </div>
            <div className="swimlane-body"></div>
          </div>
          
          <div className="swimlane-container">
              <div className="swimlane-head unselectable ellipses-text">
                In Progress
              </div>
            <div className="swimlane-body"></div>
          </div>
          
          <div className="swimlane-container">
              <div className="swimlane-head unselectable ellipses-text">
                Test
              </div>
            <div className="swimlane-body"></div>
          </div>

          <div className="swimlane-container">
              <div className="swimlane-head unselectable ellipses-text">
                Done
              </div>
            <div className="swimlane-body"></div>
          </div>
      
        </div>
      
      </div>


    </div>
  );
}

export default App;


```

</p>
</details>
... and the following css;
<details><summary>click here to see the css.</summary>
<p>

```css
html, body {
  margin: 0;
  padding: 0;
  height: 100%;
  width: 100%;
  font-family: Arial, Helvetica, sans-serif;
  color: #606060;
  box-sizing: border-box;
  overflow: hidden;
}

/** root and app are the main container classes.
  * They takes up the height of the browser, and takes up 
  * 100% browser width by default (as it is a block).
  * They also sets the application background colour. 
  */

#root {
  height: 100%;
}
.App {
  height:100%;
  background-color: #f2f2f5;
}

/** board is the swimlanes container. It is a flexbox column 
  * and this forces the header and swimlane container to
  * stack vertically.
  * NB - note that board has max-width set to fit-content 
  * so that .header-bar does not extend to the full browser 
  * width.
  */ 
.board {
  display:flex;
  flex-direction: column;
  height: 100%;
  margin: 0 4px 0 4px;
  max-width: fit-content;       /* chrome */
  max-width: -moz-max-content;  /* firefox */
  max-width: intrinsic;         /* Safari? */
}

/** .header-bar is the application header.
  * It floats the board title to the left and and menu buttons 
  * to the right, all done via a flexbox with 
  * justify-content: space-between.
  */
.header-bar {
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  padding-top: 2px;
  padding-bottom: 2px;
  padding-left: 0;
  padding-right: 0;
}

/** container for a row of buttons that lie immediately 
  * adjacent to each other. Rounds the leftmost and rightmost
  * edges.
  */

.button-group {
  display:flex;
  flex-shrink: 0;
  flex-direction: row;
}

.button-group .btn {
  border: 1px solid #dedede;
  border-right: none;
  min-width: 60px;
  min-height: 2.5em;
  padding-left: 1.5em;
  padding-right: 1.5em;
  color: #606060;
  background-color: #ffffff; 
}
.button-group .btn:hover {
  background-color: #c7dbe4;
}
.button-group .btn:focus {
  outline:0;
}

.button-group .btn:first-child {
  border-right: none; 
  border-radius: 12px 0 0 12px;
}
.button-group .btn:last-child {
  border-right: 1px solid #dedede; 
  border-radius: 0 12px 12px 0;
}

.button-group .icon {
  background-repeat: no-repeat;
  background-position: 1em 45%;
  background-size: 1em;
  padding-left:2.5em;
}
.button-group .icon.edit {
  background-image: url('./assets/svg/md-create.svg');
}

.button-group .icon.reports {
  background-image: url('./assets/svg/md-trending-up.svg');
}

.header-bar .button-group {
  margin-top: auto;
  margin-bottom: auto;
}

/** swimlanes is the class for the container that  contains 
  * the individual swimlane columns. This container has the 
  * vertical scrollbar via overflow: auto.
  * swimlane-container is the individual board columns.
  * They are a simple flexbox column with minimum width of 
  * 300px. The hover effect (where the columns appear to 
  * get a little larger on rollover) is done by setting
  * the border transparent on rollover.
  */
.swimlanes {
  display: flex;
  height: 100%;
  flex-direction: row; /* default */
  flex-wrap: nowrap;
  overflow:auto;
  padding: 0;
  margin: 0;
  scrollbar-width: thin; /* for Firefox */
}
.swimlane-container {
  display: flex;
  flex-direction: column;
  min-width: 300px;
  max-width: 300px;
  min-height: 300px;
  border: 2px solid #dedede;
  border-radius: 10px;
  margin: 5px 10px 5px 10px;
  background-color: #fdfcff;
  transition: all .3s;
  overflow: hidden;
  padding-top: 2px;
}
.swimlane-container:first-child {
  margin-left: 0;
}
.swimlane-container:last-child {
  margin-right: 0;
}
.swimlane-container:hover {
  background-color: #ffffff;
  box-shadow: 0px 2px 8px rgba(0, 0, 0, 0.2);
  border-color: transparent;
  padding-top: 0;
}

.swimlane-head {
  height: 48px;
  flex-shrink: 0;
  position: relative;
  line-height: 48px;
  border-bottom: 1px solid #e9e9e9;
  vertical-align: middle;
  overflow: hidden;
  padding: 0 24px;
}
.swimlane-body {
  position: relative;
  display: flex;
  flex-direction: column;
  padding: 0 24px;
  overflow-x: hidden;
  overflow-y: auto;
  flex-basis: 100%;
  margin-top: 2px;
  margin-bottom: 2px;
}

.story-card {
  position: relative;
  flex-shrink: 0;
  z-index: 1;
  background-color: #fffff5;
  padding: 10px 10px 10px 10px;
  margin: 0 0 10px 0;
  overflow: hidden;
  border: 1px solid #d5d5cf;
  border-left: 8px solid #60cc60;
  border-radius: 0px;
  box-shadow: 0px 1px 4px rgba(0, 0, 0, 0.1);
  transition: opacity 0.5s;
}

/** scrollbar styling.
  * Note this only works for Chrome. For other browsers, we 
  * can only use scrollbar-width: thin (see .swimlanes). 
  */
::-webkit-scrollbar {
  width: 10px;
  height:10px;
}
::-webkit-scrollbar-track {
  -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
  box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
  border-radius: 10px;
}
::-webkit-scrollbar-thumb {
  border-radius: 10px;
  background:#aaa;
}

.ellipses-text {
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;

}


/*used to make headings and text unselectable.*/
.unselectable {
  user-select: none;
  cursor: default;
}

```

</p>
</details>

(its all heavily commented)

In the next talk, we will finish ingesting by splitting out the HTML into components, and creating an initial static data flow using props.


