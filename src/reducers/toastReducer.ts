// Data classes
import ToastVO from "../data/ToastVO";
// Interfaces
import IToastVO, { ToastInitialiser } from "../data/IToastVO"
import { Action } from "../helpers/TAction";
import ToastData from "./TToastData";

const ADD_TOAST = "addToast";
const REMOVE_TOAST = "removeToast";

const toastReducer = (state: IToastVO[], action: Action): IToastVO[] => {
  const actionType: string = action.type;
  const data: ToastData = action.data;
  let nextState: IToastVO[];

  console.log("toastReducer saw ", action.forHumans);
  switch (actionType) {
    case ADD_TOAST: {
      const level: number = data.level || ToastVO.TOAST_LEVEL;
      const message: string = data.message || "";
      if (level >= ToastVO.TOAST_LEVEL && !isDuplicateToast(message, state)) {
        const toast: IToastVO = new ToastVO(data as ToastInitialiser);
        nextState = [toast].concat(state);
        return nextState;
      }
    }
      return state;

    case REMOVE_TOAST:
      const deleteId = data.id;
      nextState = state.filter((toast: IToastVO) => toast.id !== deleteId);
      return nextState;

    default:
      return state;
  }
};


const isDuplicateToast = (msg: string, state: IToastVO[]): boolean => {
  // hardcode next line if you want to see all toasts.
  //return false;
  return state.some((toast:IToastVO) => toast.message === msg);
}

export { toastReducer, ADD_TOAST, REMOVE_TOAST };


