import React, { useEffect, useReducer } from "react";
import PropTypes from "prop-types";
import { toBeInTheDocument } from "@testing-library/jest-dom";
import {
  render,
  screen,
  getByText,
  cleanup,
  within,
} from "@testing-library/react";

import { getNewAction } from "../../helpers/utils";
import {
  swimlanesReducer,
  ADD_SWIMLANE,
  REMOVE_SWIMLANE,
  EDIT_SWIMLANE,
  ADD_SWIMLANES,
} from "../swimlanesReducer";

const swimlanes = [
  {
    title: "Swimlane 0",
    id: "#1564m7s2jsl",
    index: 0,
  },
  {
    title: "Swimlane 1",
    id: "#1075qkb00o4",
    index: 1,
  },
  {
    title: "Swimlane 2",
    id: "#227btyu14ex",
    index: 2,
  },
];

/* Test component */
function TestSwimlanes({ action }) {
  const [swimlanes, dispatchSwimlanes] = useReducer(swimlanesReducer, []);

  useEffect(() => {
    if (action) {
      dispatchSwimlanes(action);
    }
  }, [action]);

  const swimlanesList = swimlanes.map((swimlane) => (
    <li key={swimlane.id} data-testid={swimlane.id}>
      {`${swimlane.id}, ${swimlane.title}, ${swimlane.index}`}
    </li>
  ));

  return <ol data-testid="listHolder">{swimlanesList}</ol>;
}
TestSwimlanes.props = {
  action: PropTypes.any,
};

function renderTestSwimlanes(action = null) {
  const { rerender } = render(<TestSwimlanes action={action} />);
  const listHolder = screen.getByTestId("listHolder");
  return { rerender, listHolder };
}

describe("It correctly returns previous state for unrecognised events", () => {
  afterEach(() => {
    cleanup();
  });

  it("should not alter state for unrecognised events", () => {
    const action = getNewAction(
      "dummy",
      swimlanes,
      "Dummy event that should not do anything"
    );
    const { listHolder } = renderTestSwimlanes(action);

    expect(listHolder.childNodes.length).toBe(0);
  });
});

describe("It correctly updates the view for add-all, insert and remove swimlanes", () => {
  afterEach(() => {
    cleanup();
  });

  it("adds all swimlanes", () => {
    const addSwimlanes = getNewAction(
      ADD_SWIMLANES,
      swimlanes,
      "Add all swimlanes"
    );
    const { listHolder } = renderTestSwimlanes(addSwimlanes);

    expect(listHolder.childNodes.length).toBe(swimlanes.length);
    // The strings representing the data for the swimlanes should be in the document
    swimlanes.forEach((swimlane) =>
      expect(screen.getByTestId(swimlane.id)).toBeInTheDocument()
    );
  });

  it("inserts a new swimlane", () => {
    // setup - add all swimlanes.
    const addSwimlanes = getNewAction(
      ADD_SWIMLANES,
      swimlanes,
      "Add all swimlanes"
    );
    const { listHolder, rerender } = renderTestSwimlanes(addSwimlanes);
    expect(listHolder.childNodes.length).toBe(swimlanes.length);

    // Add a new swimlane
    const addSwimlane = getNewAction(
      ADD_SWIMLANE,
      { addAfterId: "#1075qkb00o4" },
      "Add a swimlane"
    );
    rerender(<TestSwimlanes action={addSwimlane} />);
    /**Before adding, we will have (swimlaneId, title, index);
     * #1564m7s2jsl, Swimlane 0, 0
     * #1075qkb00o4, Swimlane 1, 1
     * #227btyu14ex, Swimlane 2, 2
     *
     * After, we expect;
     * #1564m7s2jsl, Swimlane 0, 0
     * #1075qkb00o4, Swimlane 1, 1
     * <----id---->, New Swimlane, 2 <---- inserted
     * #227btyu14ex, Swimlane 2, 3
     */
    //screen.debug();
    const rendered = listHolder.childNodes;
    expect(within(rendered[0]).getByText("#1564m7s2jsl, Swimlane 0, 0"));
    expect(within(rendered[1]).getByText("#1075qkb00o4, Swimlane 1, 1"));
    expect(within(rendered[2]).getByText(/New Swimlane, 2/i));
    expect(within(rendered[3]).getByText("#227btyu14ex, Swimlane 2, 3"));
  });

  it("handles a special case: insert with non=existent insert-after id", () => {
    // setup - add all swimlanes.
    const addSwimlanes = getNewAction(
      ADD_SWIMLANES,
      swimlanes,
      "Add all swimlanes"
    );
    const { listHolder, rerender } = renderTestSwimlanes(addSwimlanes);
    expect(listHolder.childNodes.length).toBe(swimlanes.length);
    
    // Attempting to add a new swimlane after a non-existing swimlane should 
    // result in no change.
    const addSwimlane = getNewAction(
      ADD_SWIMLANE,
      { addAfterId: "#IDontExist" },
      "Add a swimlane"
    );
    rerender(<TestSwimlanes action={addSwimlane} />);
    expect(listHolder.childNodes.length).toBe(swimlanes.length);
  });

  it("edits a swimlane", () => {
    const addSwimlanes = getNewAction(
      ADD_SWIMLANES,
      swimlanes,
      "Add all swimlanes"
    );
    const { listHolder, rerender } = renderTestSwimlanes(addSwimlanes);
    expect(listHolder.childNodes.length).toBe(swimlanes.length);
    expect(screen.getByText("#1075qkb00o4, Swimlane 1, 1")).toBeInTheDocument();

    // Change a swimlane title, confirm the old title is no longer seen, and new title appears.
    const editSwimlane = getNewAction(
      EDIT_SWIMLANE,
      {
        id: "#1075qkb00o4",
        change: { title: "New title" },
      },
      "Edit a swimlane"
    );
    rerender(<TestSwimlanes action={editSwimlane} />);
    expect(
      screen.queryByText("#1075qkb00o4, Swimlane 1, 1")
    ).not.toBeInTheDocument();
    expect(screen.getByText("#1075qkb00o4, New title, 1")).toBeInTheDocument();
  });

  it("removes a swimlane", () => {
    const addSwimlanes = getNewAction(
      ADD_SWIMLANES,
      swimlanes,
      "Add all swimlanes"
    );
    const removeSwimlane = getNewAction(
      REMOVE_SWIMLANE,
      {
        id: "#1075qkb00o4",
      },
      "Remove a swimlane"
    );
    const { listHolder, rerender } = renderTestSwimlanes(addSwimlanes);
    expect(listHolder.childNodes.length).toBe(swimlanes.length);
    expect(screen.getByText("#1075qkb00o4, Swimlane 1, 1")).toBeInTheDocument();

    rerender(<TestSwimlanes action={removeSwimlane} />);
    expect(listHolder.childNodes.length).toBe(swimlanes.length-1);
    expect(screen.queryByText("#1075qkb00o4, Swimlane 1, 1")).not.toBeInTheDocument();
  });
});
