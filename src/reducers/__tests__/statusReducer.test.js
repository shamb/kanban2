import React, { useReducer } from "react";
import PropTypes from "prop-types";
import { toBeInTheDocument } from "@testing-library/jest-dom";
import {
  render,
  screen,
  fireEvent,
  getByText,
} from "@testing-library/react";

import { getNewAction } from "../../helpers/utils";
import { statusReducer, TOGGLE_EDIT_MODE } from "../statusReducer";

/* Test component */
function TestStatus({ action }) {
  const [status, dispatchStatus] = useReducer(statusReducer, {isEditMode:false});

  const changeStatus = () => {
    dispatchStatus(action);
  };

  return (
    <p data-testid="textHolder" onClick={changeStatus}>
      {status.isEditMode.toString()}
    </p>
  );
}
TestStatus.props = {
  changeAction: PropTypes.any.isRequired,
};

describe("It correctly returns previous state for unrecognised events", () => {

  it("should not change for unrecognised events", () => {
    const action = getNewAction("dummy", {}, "Dummy event that should not do anything");
    render(<TestStatus action={action} />);

    expect(screen.getByText("false")).toBeInTheDocument();
    fireEvent.click(screen.getByTestId("textHolder"));
    expect(screen.getByText("false")).toBeInTheDocument();
  });
});

describe("It toggles status", () => {
  beforeEach(() => {
    const action = getNewAction(TOGGLE_EDIT_MODE, {}, "Toggled status");
    render(<TestStatus action={action} />);
  });

  it("has the correct initial value", () => {
    expect(screen.getByText("false")).toBeInTheDocument();
  });

  it("updates the view on change", () => {
    fireEvent.click(screen.getByTestId("textHolder"));
    expect(screen.getByText("true")).toBeInTheDocument();
    fireEvent.click(screen.getByTestId("textHolder"));
    expect(screen.getByText("false")).toBeInTheDocument();
  });
});
