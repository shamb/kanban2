import React, { useEffect, useReducer } from "react";
import PropTypes from "prop-types";
import { toBeInTheDocument } from "@testing-library/jest-dom";
import {
  render,
  screen,
  getByText,
  cleanup,
  within,
} from "@testing-library/react";

import { getNewAction } from "../../helpers/utils";
import {
  storiesReducer,
  ADD_STORY,
  REMOVE_STORY,
  MOVE_STORY,
  ADD_STORIES,
} from "../storiesReducer";

const stories = [
  {
    id: "#1adj8dgfum1",
    parentId: "#1564m7s2jsl",
    title: "Test title 1",
    body: "Test body 1",
    index: 0,
  },
  {
    id: "#22bnkb62nl7",
    parentId: "#1564m7s2jsl",
    title: "Test title 1",
    body: "Test body 1",
    index: 1,
  },
  {
    id: "#1hppt7he31l",
    parentId: "#1564m7s2jsl",
    title: "Test title 1",
    body: "Test body 1",
    index: 2,
  },
];

/* Test component */
function TestStories({ action=null }) {
  const [stories, dispatchStories] = useReducer(storiesReducer, []);

  useEffect(() => {
    if (action) {
      dispatchStories(action);
    }
  }, [action]);

  const storiesList = stories.map((story) => (
    <li key={story.id} data-testid={story.id}>
      {`${story.id}, ${story.parentId}, ${story.title}, ${story.body}, ${story.index}`}
    </li>
  ));

  return (
    <ol data-testid="listHolder">
      {storiesList}
    </ol>
  );
}
TestStories.props = {
  action: PropTypes.any,
};

function renderTestStories(action = null) {
  const { rerender } = render(<TestStories action={action} />);
  const listHolder = screen.getByTestId("listHolder");
  return { rerender, listHolder };
}

describe("It correctly returns previous state for unrecognised events", () => {
  afterEach(() => {
    cleanup();
  });

  it("should not alter state for unrecognised events", () => {
    const action = getNewAction(
      "dummy",
      stories,
      "Dummy event that should not do anything"
    );
    const { listHolder } = renderTestStories(action);

    expect(listHolder.childNodes.length).toBe(0);
  });
});

describe("It correctly updates the view for add, move and remove stories", () => {
  afterEach(() => {
    cleanup();
  });

  it("adds all stories", () => {
    const addStories = getNewAction(ADD_STORIES, stories, "Add all stories");
    const { listHolder } = renderTestStories(addStories);

    //screen.debug();
    expect(listHolder.childNodes.length).toBe(stories.length);
    // The strings representing the data for the stories should be in the document
    stories.forEach((story) =>
      expect(screen.getByTestId(story.id)).toBeInTheDocument()
    );
  });

  it("adds stories", () => {
    const addStory1 = getNewAction(
      ADD_STORY,
      {
        parentId: "#1564m7s2jsl",
        title: "Added title 1",
        body: "Added body 1",
      },
      "Add a story"
    );
    const addStory2 = getNewAction(
      ADD_STORY,
      {
        parentId: "#1adj8dgfum1",
        title: "Added title 2",
        body: "Added body 2",
      },
      "Add a story"
    );
    const { listHolder, rerender } = renderTestStories();

    expect(listHolder.childNodes.length).toBe(0);

    // Add two stories
    rerender(<TestStories action={addStory1} />);
    rerender(<TestStories action={addStory2} />);
    //screen.debug();
    expect(listHolder.childNodes.length).toBe(2);
  });

  it("moves stories", () => {
    const addStories = getNewAction(ADD_STORIES, stories, "Add all stories");
    const { rerender, listHolder } = renderTestStories(addStories);

    expect(listHolder.childNodes.length).toBe(stories.length);
    /**Move the second story in stories to a new swimlane. We will initially
     * have three stories with id/indexes;
     * #1adj8dgfum1  0
     * #22bnkb62nl7  1
     * #1hppt7he31l  2
     *
     * We are simulating #22bnkb62nl7 being moved to another swimlane after
     * the current one, so the setup after the move will be;
     * #1adj8dgfum1  0
     * #1hppt7he31l  1
     * #22bnkb62nl7  0
     */
    const from = {
      storyId: "#22bnkb62nl7",
      storyIndex: 1,
      swimlaneId: "#1564m7s2jsl",
    };
    const to = {
      storyId: "#22bnkb62nl7",
      storyIndex: 0,
      swimlaneId: "#2564m7s2jsl",
    };
    const swimlaneList = ["#1564m7s2jsl", "#2564m7s2jsl"];
    const moveStory = getNewAction(MOVE_STORY, { from, to, swimlaneList });
    rerender(<TestStories action={moveStory} />);
    //screen.debug()
    // Check positions 0, 1, 2
    const rendered = listHolder.childNodes;
    expect(
      within(rendered[0]).getByText(
        "#1adj8dgfum1, #1564m7s2jsl, Test title 1, Test body 1, 0"
      )
    ).toBeInTheDocument();
    expect(
      within(rendered[1]).getByText(
        "#1hppt7he31l, #1564m7s2jsl, Test title 1, Test body 1, 1"
      )
    ).toBeInTheDocument();
    expect(
      within(rendered[2]).getByText(
        "#22bnkb62nl7, #2564m7s2jsl, Test title 1, Test body 1, 0"
      )
    ).toBeInTheDocument();
  });

  it("doesn't make a change when the user moves a story to the same position (no-op)", () => {
    const addStories = getNewAction(ADD_STORIES, stories, "Add all stories");
    const { rerender, listHolder } = renderTestStories(addStories);

    expect(listHolder.childNodes.length).toBe(stories.length);
    //screen.debug();

    const from = {
      storyId: "#22bnkb62nl7",
      storyIndex: 1,
      swimlaneId: "#1564m7s2jsl",
    };
    const to = {
      storyId: "#22bnkb62nl7",
      storyIndex: 1,
      swimlaneId: "#1564m7s2jsl",
    };
    const swimlaneList = ["#1564m7s2jsl", "#2564m7s2jsl"];
    const moveStory = getNewAction(MOVE_STORY, { from, to, swimlaneList });
    rerender(<TestStories action={moveStory} />);
    //screen.debug();
    // All stories should appear, and in the correct order.
    const rendered = listHolder.childNodes;
    expect(
      within(rendered[0]).getByText(
        "#1adj8dgfum1, #1564m7s2jsl, Test title 1, Test body 1, 0"
      )
    ).toBeInTheDocument();
    expect(
      within(rendered[1]).getByText(
        "#22bnkb62nl7, #1564m7s2jsl, Test title 1, Test body 1, 1"
      )
    ).toBeInTheDocument();
    expect(
      within(rendered[2]).getByText(
        "#1hppt7he31l, #1564m7s2jsl, Test title 1, Test body 1, 2"
      )
    ).toBeInTheDocument();
  });

  it("deletes stories", () => {
    const addStories = getNewAction(ADD_STORIES, stories, "Add all stories");
    const deleteStory = getNewAction(
      REMOVE_STORY,
      { id: "#22bnkb62nl7", parentId: "#1564m7s2jsl" },
      "delete middle story"
    );
    const { rerender, listHolder } = renderTestStories(addStories);

    expect(listHolder.childNodes.length).toBe(stories.length);

    /**Delete a story.
     * We will initially have stories arranged as follows (storyId/index);
     * #1adj8dgfum1  0
     * #22bnkb62nl7  1
     * #1hppt7he31l  2
     *
     * After deleting #22bnkb62nl7, the remaining two stories will be re-
     * indexed to give us;
     * #1adj8dgfum1  0
     * #1hppt7he31l  1
     *
     */
    rerender(<TestStories action={deleteStory} />);
    // screen.debug();
    const rendered = listHolder.childNodes;
    expect(
      within(rendered[0]).getByText(
        "#1adj8dgfum1, #1564m7s2jsl, Test title 1, Test body 1, 0"
      )
    ).toBeInTheDocument();
    expect(
      within(rendered[1]).getByText(
        "#1hppt7he31l, #1564m7s2jsl, Test title 1, Test body 1, 1"
      )
    ).toBeInTheDocument();
  });
});
