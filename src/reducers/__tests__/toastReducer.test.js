import React, { useEffect, useReducer } from "react";
import PropTypes from "prop-types";
import { toBeInTheDocument } from "@testing-library/jest-dom";
import {
  render,
  screen,
  getByText,
  cleanup,
  within,
  queryAllByText,
} from "@testing-library/react";

import { getNewAction } from "../../helpers/utils";
import { toastReducer, ADD_TOAST, REMOVE_TOAST } from "../toastReducer";
import ToastVO from "../../data/ToastVO";

const toasts = [
  {
    message: "Toast message 1",
    type: ToastVO.INFO,
    mustAccept: false,
    duration: 3000,
    level: ToastVO.TOAST_LEVEL_NORMAL,
  },
  {
    message: "Toast message 2",
    type: ToastVO.WARNING,
    mustAccept: true,
    duration: 3000,
    level: 3,
  },
];

/* Test component */
function TestToasts({ action = null }) {
  const [toasts, dispatchToasts] = useReducer(toastReducer, []);

  useEffect(() => {
    if (action) {
      dispatchToasts(action);
    }
  }, [action]);

  const toastList = toasts.map((toast) => (
    <li key={toast.id} data-testid={toast.id}>
      {`${toast.id}, ${toast.type}, ${toast.message}, ${toast.mustAccept}, ${toast.duration}, ${toast.level}`}
    </li>
  ));

  return <ol data-testid="listHolder">{toastList}</ol>;
}
TestToasts.props = {
  action: PropTypes.any,
};

function renderTestToasts(action = null) {
  const { rerender } = render(<TestToasts action={action} />);
  const listHolder = screen.getByTestId("listHolder");
  return { rerender, listHolder };
}

describe("It correctly returns previous state for unrecognised events", () => {
  afterEach(() => {
    cleanup();
  });

  it("should not alter state for unrecognised events", () => {
    const action = getNewAction(
      "dummy",
      {
        message: "I should not appear.",
        duration: 1000,
        type: ToastVO.INFO,
        level: ToastVO.TOAST_LEVEL_DEBUG,
      },
      "dummy action"
    );
    const { listHolder } = renderTestToasts(action);

    expect(listHolder.childNodes.length).toBe(0);
  });
});

describe("It correctly updates the view for add and remove toasts", () => {
  afterEach(() => {
    cleanup();
  });

  it("adds and removes toasts", () => {
    const addToast1 = getNewAction(ADD_TOAST, toasts[0], "New toast");
    const addToast2 = getNewAction(ADD_TOAST, toasts[1], "Another new toast");
    const { rerender, listHolder } = renderTestToasts(addToast1);
    let childNodes;

    // Should be one toast at start.
    expect(listHolder.childNodes.length).toBe(1);
    expect(screen.getByText(/info, Toast message 1, false, 3000, 2/i));

    // Add another and confirm there are now two, newest first.
    rerender(<TestToasts action={addToast2} />);
    childNodes = listHolder.childNodes;
    expect(
      within(childNodes[0]).getByText(
        /warning, Toast message 2, true, 3000, 3/i
      )
    );
    expect(
      within(childNodes[1]).getByText(/info, Toast message 1, false, 3000, 2/i)
    );

    // Remove a toast and confirm it disappears.
    // NB - we don't know the toast.id, but can get it from the view data-testid attribute.
    const removeId = childNodes[0].getAttribute("data-testid");
    const removeToast = getNewAction(
      REMOVE_TOAST,
      { id: removeId },
      "Remove toast"
    );
    rerender(<TestToasts action={removeToast} />);
    expect(childNodes.length).toBe(1);
    expect(
      screen.queryAllByText(/warning, Toast message 2, true, 3000, 3/i).length
    ).toBe(0);
  });

  describe("it handles edge-case functionality", () => {
    afterEach(() => {
      cleanup();
    });

    it("doesn't add a toast that is below the current level", () => {
      const veryLowPriorityToast = {
        message: "Toast message 1",
        type: ToastVO.INFO,
        mustAccept: false,
        duration: 3000,
        level: -1,
      };
      const addToast = getNewAction(
        ADD_TOAST,
        veryLowPriorityToast,
        "New toast"
      );
      const { listHolder } = renderTestToasts(addToast);

      expect(listHolder.childNodes.length).toBe(0);
    });
    
    it("doesn't add duplicates", () => {
      const addToast = getNewAction(ADD_TOAST, toasts[0], "New toast");
      const { rerender, listHolder } = renderTestToasts(addToast);
      rerender(<TestToasts action={addToast} />);
      
      expect(listHolder.childNodes.length).toBe(1);
    });

    it("adds a default message if it is not provided", () => {
      const billyNoMessage = {
        type: ToastVO.INFO,
        mustAccept: false,
        duration: 3000,
        level: ToastVO.TOAST_LEVEL_NORMAL,
      };
      const addToast = getNewAction(ADD_TOAST, billyNoMessage, "New toast")
      const { listHolder } = renderTestToasts(addToast);
      
      expect(listHolder.childNodes.length).toBe(1)
      expect(screen.getByText(/info, , false, 3000, 2/i)).toBeInTheDocument();
    });
  });
});
