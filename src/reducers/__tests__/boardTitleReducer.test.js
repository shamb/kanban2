import React, { useEffect, useReducer } from "react";
import PropTypes from "prop-types";
import { toBeInTheDocument } from "@testing-library/jest-dom";
import {
  render,
  screen,
  getByText, 
  cleanup
} from "@testing-library/react";

import { getNewAction } from "../../helpers/utils";
import { boardTitleReducer, CHANGE_TITLE } from "../boardTitleReducer";

/* Test component */
function TestBoardTitle({ action }) {
  const [boardTitle, dispatchBoardTitle] = useReducer(boardTitleReducer, "initial");

  useEffect(() => {
    if (action) {
      dispatchBoardTitle(action);
    }
  }, [action]);

  return (
    <p data-testid="textHolder">
      {boardTitle}
    </p>
  );
}
TestBoardTitle.props = {
  action: PropTypes.any,
};

function renderTestBoardTitle(action = null) {
  const { rerender } = render(<TestBoardTitle action={action} />);
  return { rerender };
}

describe("It correctly returns previous state for unrecognised events", () => {
  afterEach(() => {
    cleanup();
  });

  it("should not change for unrecognised events", () => {
    const action = getNewAction(
      "dummy",
      { boardTitle: "new title" },
      "Dummy event that should not do anything"
    );
    const { rerender } = renderTestBoardTitle(action);

    expect(screen.getByText("initial")).toBeInTheDocument();
    const action2 = getNewAction(
      "dummy2",
      { boardTitle: "new title2" },
      "Dummy event that should not do anything"
    );
    rerender(<TestBoardTitle action={action2} />);
    expect(screen.getByText("initial")).toBeInTheDocument();
  });
});

describe("It changes board title", () => {
  afterEach(() => {
    cleanup();
  });

  it("has the correct initial value", () => {
    renderTestBoardTitle();
    expect(screen.getByText("initial")).toBeInTheDocument();
  });

  it("updates the view on change", () => {
    const action = getNewAction(
      CHANGE_TITLE,
      { boardTitle: "new title" },
      "Change the title"
    );
    renderTestBoardTitle(action);
    expect(screen.getByText("new title")).toBeInTheDocument();
  });
});

