import { deepClone } from "../helpers/utils";
// Interfaces
import { Action } from "../helpers/TAction";
import IStatus from "./TStatus";

const TOGGLE_EDIT_MODE:string = "toggleEditMode";

const statusReducer = (state:any, action: Action):IStatus => {
  const actionType:string = action.type;
  //const data = action.data;
  let nextState:IStatus;

  console.log("statusReducer saw ", action.forHumans);
  switch (actionType) {
    case TOGGLE_EDIT_MODE:
      nextState = deepClone(state);
      nextState.isEditMode = !nextState.isEditMode;
      return nextState;

    default:
      return state;
  }
};

export { statusReducer, TOGGLE_EDIT_MODE };
