/* istanbul ignore file */
type StoryLocation = {
  storyId:string,
  storyIndex:number,
  swimlaneId:string,
}

export default StoryLocation;