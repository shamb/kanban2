const setDispatchers = (theDispatchers:any) => {
  // Only allow dispatchers to be set once.
  // This avoids them being overwritten in error.
  if (!hasSetDispatchers) {
    dispatchers = theDispatchers;
    hasSetDispatchers = true;
  }
}

const getDispatchers = () => dispatchers;

let dispatchers:any = {};
let hasSetDispatchers:boolean = false;

export { getDispatchers, setDispatchers };