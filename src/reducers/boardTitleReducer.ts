// Interfaces
import { Action } from "../helpers/TAction";

const CHANGE_TITLE: string = "changeTitle";

const boardTitleReducer = (state: any, action: Action):string => {
  const actionType: string = action.type;
  const data: any = action.data;
  let nextState: string;

  console.log("boardTitleReducer saw ", action.forHumans);
  switch (actionType) {

    case CHANGE_TITLE:
      nextState = data.boardTitle;
      return nextState;

    default:
      return state;
  }
}

export { boardTitleReducer, CHANGE_TITLE };