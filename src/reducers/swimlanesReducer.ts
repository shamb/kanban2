// Data classes
import SwimlaneVO from "../data/SwimlaneVO";
// Interfaces
import ISwimlaneVO, { SwimlaneInitialiser } from "../data/ISwimlaneVO";
import { Action } from "../helpers/TAction";

const ADD_SWIMLANE:string = "addSwimlane";
const REMOVE_SWIMLANE:string = "removeSwimlane";
const EDIT_SWIMLANE:string = "editSwimlane";
const ADD_SWIMLANES:string = "addSwimlanes";

const swimlanesReducer = (state:ISwimlaneVO[], action:Action):ISwimlaneVO[] => {
  const actionType:string = action.type;
  const data:any = action.data;
  let nextState: ISwimlaneVO[];

  console.log("swimlaneReducer saw ", action.forHumans);
  switch (actionType) {
    case ADD_SWIMLANE:
      // insert then re-index
      nextState = insertAfterId(data.addAfterId, new SwimlaneVO(data), state);
      nextState = reIndex(nextState);
      return nextState;

    case REMOVE_SWIMLANE:
      const deleteId = data.id;
      // delete then re-index.
      nextState = state.filter((swimlane:ISwimlaneVO) => swimlane.id !== deleteId);
      nextState = reIndex(nextState);
      return nextState;

    case EDIT_SWIMLANE:
      const changeId = data.id;
      // change id.
      nextState = state.map((swimlane:ISwimlaneVO) =>
        swimlane.id === changeId
          ? SwimlaneVO.update(swimlane, data.change)
          : swimlane
      );
      return nextState;

    case ADD_SWIMLANES:
      // turn the incoming data into Swimlane instances.
      nextState = data.map((swimlane:SwimlaneInitialiser) => new SwimlaneVO(swimlane));
      return nextState;

    default:
      return state;
  }
};

const insertAfterId = (id:string, insert:ISwimlaneVO, swimlanes:ISwimlaneVO[]):ISwimlaneVO[] => {
  let insertIndex:number = 0;
  let hasFound:boolean;

  hasFound = swimlanes.some((swimlane:ISwimlaneVO, index:number) => {
    if (swimlane.id === id) {
      insertIndex = index;
      return true;
    }
    return false;
  });
  if (hasFound) {
    return [
      ...swimlanes.slice(0, insertIndex + 1),
      insert,
      ...swimlanes.slice(insertIndex + 1)
    ];
  }
  return swimlanes;
};

const reIndex = (swimlanes:ISwimlaneVO[]):ISwimlaneVO[] => {
  return swimlanes.map((swimlane, index) => {
    if (swimlane.index !== index) {
      return SwimlaneVO.update(swimlane, { index });
    }
    return swimlane;
  });
};

export { swimlanesReducer, ADD_SWIMLANE, REMOVE_SWIMLANE, EDIT_SWIMLANE, ADD_SWIMLANES };
