import StoryVO from "../data/StoryVO";
// Interfaces
import IStoryVO, { StoryInitialiser} from "../data/IStoryVO";
import { Action } from "../helpers/TAction";
import AssociativeArray from "../helpers/TAssociativeArray";
import StoryLocation from "./TStoryLocation"

const ADD_STORY: string = "addStory";
const REMOVE_STORY: string = "removeStory";
const MOVE_STORY: string = "moveStory";
const ADD_STORIES: string = "addStories";

const storiesReducer = (state: IStoryVO[], action: Action):IStoryVO[] => {
  const actionType: string = action.type;
  const data: any = action.data;
  let nextState: IStoryVO[];

  console.log("storiesReducer saw ", action.forHumans);
  switch (actionType) {
    case ADD_STORY:
      // stories are always added to the top of the first swimlane
      nextState = [new StoryVO(data as StoryInitialiser), ...state];
      nextState = reIndex(nextState, [data.parentId]);
      return nextState;

    case MOVE_STORY:
      nextState = moveStory(state, data.from, data.to, data.swimlaneList);
      nextState = reIndex(nextState, [
        data.from.swimlaneId,
        data.to.swimlaneId
      ]);
      return nextState;

    case REMOVE_STORY:
      const deleteId = data.id;
      nextState = state.filter((story: IStoryVO) => story.id !== deleteId);
      nextState = reIndex(nextState, [data.parentId]);
      return nextState;

    case ADD_STORIES:
      // turn the incoming data into StoryVO instances.
      nextState = data.map((story: IStoryVO) => new StoryVO(story as StoryInitialiser));
      return nextState;

    default:
      return state;
  }
};

const moveStory = (state: IStoryVO[], from: StoryLocation, to: StoryLocation, swimlaneList: string[]):IStoryVO[] => {
  let nextState: IStoryVO[] = []
  let movedStory: IStoryVO;
  let swimlaneStartIndex: number;
  let insertIndex: number;
  let foundStory: boolean;

  if (from.swimlaneId === to.swimlaneId && from.storyIndex === to.storyIndex) {
    // Nothing has been moved; make no changes.
    return state;
  }
  // Find the 'from' story and remove it.
  foundStory = state.some((story: IStoryVO, index: number) => {
    const isFound = from.storyId === story.id;

    if (isFound) {
      movedStory = StoryVO.update(story, {
        parentId: to.swimlaneId,
        index: to.storyIndex
      });
      nextState = [...state.slice(0, index), ...state.slice(index + 1)];
    }
    return isFound;
  });
  // Get the start index of the 'to' swimlane.
  swimlaneStartIndex = getSwimlaneStartIndex(
    nextState,
    swimlaneList,
    to.swimlaneId
  );
  if (foundStory) {
    // Insert the moved story within the 'to' swimlane.
    // NB - ts-ignore to suppress "Variable 'movedStory' is used before being assigned" / TS2454
    insertIndex = swimlaneStartIndex + to.storyIndex;
    nextState = [
      ...nextState.slice(0, insertIndex),
      // @ts-ignore
      movedStory,
      ...nextState.slice(insertIndex)
    ];
  }
  return nextState;
};

const getSwimlaneStartIndex = (state: IStoryVO[], swimlaneList: string[], swimlaneId: string):number => {
  let startIndex = 0;
  const swimlaneIndex = swimlaneList.indexOf(swimlaneId);

  // returns all stories in the swimlane with id swimlaneId
  state.some((story:IStoryVO) => {
    let hasFoundSwimlaneStart = false;
    if (swimlaneList.indexOf(story.parentId) < swimlaneIndex) {
      startIndex += 1;
    } else {
      hasFoundSwimlaneStart = true;
    }
    return hasFoundSwimlaneStart;
  });
  return startIndex;
};

const reIndex = (stories: IStoryVO[], swimlaneIds: string[]):IStoryVO[] => {
  let indexes: AssociativeArray = {};
  let indexedStories:Array<StoryVO>;

  // NB - indexes only contains the swimlanes involved in the drag drop,
  // so only 1 or 2.
  swimlaneIds.forEach(swimlaneId => {
    indexes[swimlaneId] = 0;
  });

  indexedStories =  stories.map(story => {
    // Sanity check for when the user drops on a non-swimlane.
    if (swimlaneIds.indexOf(story.parentId) !== -1) {
      // Is the story inside a swimlane involved in the change?
      if (story.index !== indexes[story.parentId]) {
        const updatedStory:StoryVO =  StoryVO.update(story, {
          index: indexes[story.parentId]++
        });
        return updatedStory;
      }
    }
    indexes[story.parentId]++
    return story;
  });
  return indexedStories;
};

export { storiesReducer, ADD_STORY, REMOVE_STORY, MOVE_STORY, ADD_STORIES };
