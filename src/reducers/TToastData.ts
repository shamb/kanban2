/* istanbul ignore file */
type ToastData = {
  message?: string,
  type?: string,
  duration?: number,
  level?: number;
  id?:string,
}

export default ToastData;