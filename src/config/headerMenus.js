const HEADER_BUTTONS = [
  {
    id: "reports",
    type: "button",
    label: "Reports",
    className: "btn icon reports"
  },
  {
    id: "editBoard",
    type:"toggle",
    label: "Edit Board",
    className: ["btn icon edit", "btn icon edit toggled"]
  }
];

export { HEADER_BUTTONS };