# Data specification

## Core
All objects have the following properties.

Property | Type | Notes
---: | :--- | :--
`id`| string |
`parentId` | string | Child elements always use the parentId to refer back to their parent. The top level object (Project) has a parentId of `null`.
`parentType` | string (enum) | One of  
`isEditMode` | Boolean |
`name` | string |
`history` | array of ChangeVO  |
`lastChange` | string (date stamp) | Used when real time updates are required.

## ChangeVO


## ProjectVO

Property | Type | Notes
---: | :--- | :---
_none_  | |

## BoardVO

Property | Type | Notes
---: | :--- | :---
`id` | string |
`parentId` | string | id of the parent project.
`isEditMode` | Boolean |
`name` | string |
`swimlanes` | array of Swimlane |
`stories` | array of Story |


### Core Properties

#### Backlog board

#### Sprint board

#### Burndown chart

## Story

