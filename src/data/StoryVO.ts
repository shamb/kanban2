import { USE_RUNTIME_TYPING } from "./voConstants";
import BaseVO from "./BaseVO";
// Interfaces
import IStoryVO, { StoryInitialiser } from "./IStoryVO";

const DEFAULT_PARENT_ID = "";
const DEFAULT_TITLE: string = "New Story";
const DEFAULT_BODY: string = "";
const DEFAULT_INDEX: number = 0;
const defaultObj: StoryInitialiser = {
  id: null,
  parentId: DEFAULT_PARENT_ID,
  title: DEFAULT_TITLE,
  body: DEFAULT_BODY,
  index: DEFAULT_INDEX,
};

// The Story Value Object.

class StoryVO extends BaseVO implements IStoryVO {
  private static readonly _className:string = "StoryVO";
  //
  // public id:string (inherited from BaseVO)
  public parentId: string;
  public title: string;
  public body: string;
  public index: number;

  constructor({
    id,
    parentId = defaultObj.parentId,
    title = defaultObj.title,
    body = defaultObj.body,
    index = defaultObj.index }: StoryInitialiser = defaultObj, checkTypes: boolean = USE_RUNTIME_TYPING) {

    super({ id });
    this.parentId = parentId;
    this.title = title;
    this.body = body;
    this.index = index;
    if (checkTypes) {
      StoryVO.checkVOForTypes(this);
    }
    Object.freeze(this);
  }

  public get className(): string {
    // NB - we can't use this.constructor.name because it can be lost in production (bundlers).
    return StoryVO._className;
  }

  public static update(instance: IStoryVO, changes: any, checkTypes: boolean = USE_RUNTIME_TYPING): IStoryVO {
    const updatedData: any = { ...instance, ...changes };
    return new StoryVO(updatedData, checkTypes);
  }

  public static checkVOForTypes(obj: any): void {
    const { parentId, title, body, index } = obj
    let errors: string[] = [...super.checkBaseData(obj)];

    // NB - id cannot be changed after creation.
    if (parentId !== null && typeof parentId !== "string") {
      errors.push("parentId should be null or a string");
    }
    if (typeof title !== "string") {
      errors.push("title should be a string");
    }
    if (typeof body !== "string") {
      errors.push("body should be a string");
    }
    if (typeof index !== "number") {
      errors.push("index should be a number");
    }
    if (errors.length > 0) {
      throw new TypeError(`StoryVO - ${errors.join(", ")}.`);
    }
  }
}

export default StoryVO;

// these exports are for testing.
export {
DEFAULT_PARENT_ID,
DEFAULT_TITLE,
DEFAULT_BODY,
DEFAULT_INDEX,
}