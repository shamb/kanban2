const USE_RUNTIME_TYPING:boolean = true;
const MINIMUM_ID_LENGTH: number = 10;

export { 
  USE_RUNTIME_TYPING,
  MINIMUM_ID_LENGTH, 
};