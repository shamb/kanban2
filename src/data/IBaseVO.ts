/* istanbul ignore file */

export default interface IBaseVO {
  id: string,
}

export interface BaseVOInitialiser {
  id: string | null;
}
