import { getId } from "../helpers/utils";
import IBaseVO, { BaseVOInitialiser } from "./IBaseVO";
import { MINIMUM_ID_LENGTH } from "./voConstants";


class BaseVO implements IBaseVO {
  public id: string;
  
  constructor({ id }:BaseVOInitialiser) {
    let theId:string = id ? id : getId();
    
    this.id = theId;
  }

  public static checkBaseData(obj: any): Array<string> {
    const { id } = obj;
    let errors: string[] = [];

    if(id.toString().trim().length < MINIMUM_ID_LENGTH) {
      errors.push("(BaseVO) id is too short");
    }
    return errors;
  }
}

export default BaseVO

