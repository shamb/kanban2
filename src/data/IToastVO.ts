/* istanbul ignore file */
export default interface IToastVO {
  className:string;
  message: string;
  type: string;
  mustAccept: boolean;
  duration: number;
  id: string;
  level: number;
}

// minimum properties a raw object used to initialise a IToastVO must have.
export type ToastInitialiser = {
  message: string;
  type: string;
  mustAccept: boolean;
  duration: number;
  id: string | null;
  level: number;
}