import { getId } from "../helpers/utils";
import { USE_RUNTIME_TYPING } from "./voConstants";
import BaseVO from "./BaseVO";
// Interfaces
import IToastVO, { ToastInitialiser } from "./IToastVO";

const TOAST_LEVEL_DEBUG: number = 0;
const TOAST_LEVEL_VERBOSE: number = 1;
const TOAST_LEVEL_NORMAL: number = 2;
const TOAST_LEVEL_TERSE: number = 3;
const TOAST_LEVEL: number = TOAST_LEVEL_DEBUG; // Change this to set default priority
const ALLOWED_LEVELS: Array<number> = [
  TOAST_LEVEL_DEBUG,
  TOAST_LEVEL_VERBOSE,
  TOAST_LEVEL_NORMAL,
  TOAST_LEVEL_TERSE
];

const DEFAULT_TOAST_DURATION: number = 3000;
const INFO: string = "info";
const WARNING: string = "warning";
const ALLOWED_TYPES: Array<string> = [INFO, WARNING];

const DEFAULT_MESSAGE: string = "";
const DEFAULT_TYPE: string = INFO;
const DEFAULT_MUST_ACCEPT: boolean = false;
const DEFAULT_TOAST_LEVEL: number = TOAST_LEVEL;
const defaultObj: ToastInitialiser = {
  message: DEFAULT_MESSAGE,
  id: null,
  type: DEFAULT_TYPE,
  mustAccept: DEFAULT_MUST_ACCEPT,
  duration: DEFAULT_TOAST_DURATION,
  level: DEFAULT_TOAST_LEVEL,
}

// The Toast Value Object.

class ToastVO extends BaseVO implements IToastVO {
  //
  public static readonly TOAST_LEVEL_DEBUG: number = TOAST_LEVEL_DEBUG;
  public static readonly TOAST_LEVEL_VERBOSE: number = TOAST_LEVEL_VERBOSE;
  public static readonly TOAST_LEVEL_NORMAL: number = TOAST_LEVEL_NORMAL;
  public static readonly TOAST_LEVEL_TERSE: number = TOAST_LEVEL_TERSE;
  public static readonly TOAST_LEVEL: number = TOAST_LEVEL_DEBUG;
  public static readonly DEFAULT_TOAST_DURATION: number = DEFAULT_TOAST_DURATION;
  public static readonly INFO: string = INFO;
  public static readonly WARNING: string = WARNING;
  private static readonly _className: string = "ToastVO";
  //
  // public id:string (inherited from BaseVO)
  public message: string;
  public type: string;
  public mustAccept: boolean;
  public duration: number;
  public level: number;

  constructor({
    id,
    message = defaultObj.message,
    type = defaultObj.type,
    mustAccept = defaultObj.mustAccept,
    duration = defaultObj.duration,
    level = defaultObj.level }: ToastInitialiser = defaultObj, checkTypes: boolean = USE_RUNTIME_TYPING) {

    super({ id });
    this.message = message;
    this.type = type;
    this.mustAccept = mustAccept;
    this.duration = duration;
    this.level = level;
    if (checkTypes) {
      ToastVO.checkVOForTypes(this);
    }
    Object.freeze(this);
  }

  public get className(): string {
    return ToastVO._className;
  }

  public static checkVOForTypes(obj: any): void {
    const { message, type, mustAccept, duration, level } = obj
    let errors: string[] = [...super.checkBaseData(obj)];

    if (typeof message !== "string" && typeof message !== "object") {
      errors.push("message should be a string or JSX object");
    }
    if (!ALLOWED_TYPES.includes(type)) {
      errors.push(`type should be one of [${ALLOWED_TYPES.toString()}]`);
    }
    if (typeof mustAccept !== "boolean") {
      errors.push("mustAccept should be a Boolean");
    }
    if (typeof duration !== "number") {
      errors.push("duration should be a number");
    }
    if (!ALLOWED_LEVELS.includes(level)) {
      errors.push(`level should be one of [${ALLOWED_LEVELS.toString()}]`);
    }
    if (errors.length > 0) {
      throw new TypeError(`ToastVO - ${errors.join(", ")}.`);
    }
  }

}

export default ToastVO;

// these exports are for testing.
export {
  DEFAULT_MESSAGE,
  DEFAULT_TYPE,
  DEFAULT_MUST_ACCEPT,
  DEFAULT_TOAST_LEVEL,
}