import ToastVO, {
  DEFAULT_MESSAGE,
  DEFAULT_TYPE,
  DEFAULT_MUST_ACCEPT,
  DEFAULT_TOAST_LEVEL,
} from "../ToastVO";

function isEqualObjects(a, b) {
  // Only works for the particular use case in this test!
  if (
    Object.getOwnPropertyNames(a).length ===
    Object.getOwnPropertyNames(b).length
  ) {
    let propName;
    for (propName in a) {
      if (propName !== "id" && a[propName] !== b[propName]) {
        return false;
      }
    }
  } else {
    return false;
  }
  return true;
}

const toast1 = {
  id: "makeSureTheIdIsLongEnough",
  message: "Toast message",
  type: ToastVO.WARNING,
  mustAccept: true,
  duration: 3000,
  level: ToastVO.TOAST_LEVEL_NORMAL,
};

const toastWithTypeErrors = {
  id: "#1adj8dgfum2",
  message: 6,
  type: "wrong",
  mustAccept: "false",
  duration: "1000",
  level: 6,
};

describe("It initialises and updates correctly", () => {
  it("returns the correct className", () => {
    expect(new ToastVO({}).className).toBe("ToastVO");
  });

  it("takes valid story data and returns the object back", () => {
    const toast = new ToastVO(toast1);
    expect(isEqualObjects(toast1, toast)).toBe(true);
  });

  it("takes and incomplete object and returns a full VO", () => {
    const toast = new ToastVO({});
    expect(toast.id).toBeTruthy();
    expect(toast.message).toBe(DEFAULT_MESSAGE);
    expect(toast.type).toBe(DEFAULT_TYPE);
    expect(toast.mustAccept).toBe(DEFAULT_MUST_ACCEPT);
    expect(toast.duration).toBe(ToastVO.DEFAULT_TOAST_DURATION);
    expect(toast.level).toBe(DEFAULT_TOAST_LEVEL);
  });

  it("is uneditable when change is attempted directly", () => {
    const toast = new ToastVO({});
    let errorMsg;
    try {
      toast.message = "some other message";
    } catch (e) {
      errorMsg = e.name;
    }
    // The title should not change.
    expect(errorMsg).toBe("TypeError");
    expect(toast.message).toBe(DEFAULT_MESSAGE);
  });
});

describe("it performs run time type checking on change", () => {
  it("throws on badly typed initialization data", () => {
    let msg;
    try {
      new ToastVO(toastWithTypeErrors);
    } catch (e) {
      msg = e.message;
    }

    //console.log(msg);
    expect(msg.includes("ToastVO")).toBe(true);
    expect(msg.includes("message should be a string or JSX object")).toBe(true);
    expect(msg.includes("type should be one of [info,warning]")).toBe(true);
    expect(msg.includes("mustAccept should be a Boolean")).toBe(true);
    expect(msg.includes("duration should be a number")).toBe(true);
    expect(msg.includes("level should be one of [0,1,2,3]")).toBe(true);
  });
});

describe("it can be forced to ignore type checking", () => {
  it("will ignore type checking on initialization when forced", () => {
    const toast = new ToastVO(toastWithTypeErrors, false);
    expect(isEqualObjects(toast, toastWithTypeErrors)).toBe(true);
  });
});
