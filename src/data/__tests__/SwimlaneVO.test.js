import { MINIMUM_ID_LENGTH } from "../voConstants";
import SwimlaneVO, { DEFAULT_TITLE, DEFAULT_INDEX} from "../SwimlaneVO";

function isEqualObjects(a, b) {
  // Only works for the particular use case here - a and b must be objects!
  if (
    Object.getOwnPropertyNames(a).length ===
    Object.getOwnPropertyNames(b).length
  ) {
    let propName;
    for (propName in a) {
      if (a[propName] !== b[propName]) {
        return false;
      }
    }
  } else {
    return false;
  }
  return true;
}

const swimlane1 = {
  id: "#1564m7s2jsl",
  title: "Swimlane title",
  index: 0,
};

const swimlaneWithTypeErrors = {
  id: "#1564m7s2jsl",
  title: true,
  index: "0",
};

describe("It initialises and updates correctly", () => {
  it("returns the correct className", () => {
    expect(new SwimlaneVO({}).className).toBe("SwimlaneVO");
  });

  it("takes valid swimlane data and returns the object back", () => {
    const swimlane = new SwimlaneVO(swimlane1);
    expect(isEqualObjects(swimlane1, swimlane)).toBe(true);
  });

  it("takes and incomplete object and returns a full VO", () => {
    const swimlane = new SwimlaneVO({});
    console.log("swimlane, ", swimlane);
    expect(swimlane.id).toBeTruthy();
    expect(swimlane.title).toBe(DEFAULT_TITLE);
    expect(swimlane.index).toBe(DEFAULT_INDEX);
  });

  it("is uneditable when change is attempted directly", () => {
    const swimlane = new SwimlaneVO({});
    let errorMsg;
    try {
      swimlane.title = "some other swimlane";
    } catch (e) {
      errorMsg = e.name;
    }
    // The title should not change.
    expect(errorMsg).toBe("TypeError");
    expect(swimlane.title).toBe(DEFAULT_TITLE);
  });

  it("creates a new instance on 'change' because it is immutable", () => {
    let swimlane = new SwimlaneVO(swimlane1);
    let swimlaneRef = swimlane;
    swimlane = SwimlaneVO.update(swimlane, { title: "Some new title" });
    // swimlane will be changed, but the ref will still point to the old version.
    expect(swimlane.title).toBe("Some new title");
    expect(swimlaneRef.title).toBe("Swimlane title");
  });
});

describe("it performs run time type checking on change", () => {
  it("throws on badly typed initialization data", () => {
    let msg;
    try {
      new SwimlaneVO(swimlaneWithTypeErrors);
    } catch (e) {
      msg = e.message;
    }
    expect(msg.includes("SwimlaneVO")).toBe(true);
    expect(msg.includes("title should be a string")).toBe(true);
    expect(msg.includes("index should be a number")).toBe(true);
  });

  it("throws on bad change data", () => {
    let swimlane = new SwimlaneVO(swimlane1);
    let msg;

    try {
      swimlane = SwimlaneVO.update(swimlane, { title: 0 });
    } catch (e) {
      msg = e.message;
    }
    expect(msg).toBe("SwimlaneVO - title should be a string.");
  });
});

describe("it can be forced to ignore type checking", () => {
  it("will ignore type checking on initialization when forced", () => {
    const swimlane = new SwimlaneVO(swimlaneWithTypeErrors, false);
    expect(isEqualObjects(swimlane, swimlaneWithTypeErrors)).toBe(true);
  });

  it("will ignore type checking on updates when forced", () => {
    let swimlane = new SwimlaneVO(swimlane1);
    swimlane = SwimlaneVO.update(swimlane, { title: 123 }, false);
    expect(swimlane.title).toBe(123);
  });
});

describe("it always returns a valid id", () => {
  it("creates an id if none is provided", () => {
    const swimlane = new SwimlaneVO({});
    expect(typeof swimlane.id).toBe("string");
    expect(swimlane.id.length >= MINIMUM_ID_LENGTH).toBe(true);
  });

  it("raises an error if the id is unsuitable", () => {
    let msg;

    try {
      new SwimlaneVO({ id: 3 });
    } catch (e) {
      msg = e.message;
    }
    expect(msg).toBe("SwimlaneVO - (BaseVO) id is too short.");
  });

  it("returns an object with the provided id if the id is valid", () => {
    const newId = "#1adj8dgfum1";
    const swimlane = new SwimlaneVO({ id: newId });
    expect(swimlane.id).toBe(newId);
  });
});
