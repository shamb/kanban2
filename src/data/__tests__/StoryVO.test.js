import { MINIMUM_ID_LENGTH } from "../voConstants";
import StoryVO, {
  DEFAULT_PARENT_ID,
  DEFAULT_TITLE,
  DEFAULT_BODY,
  DEFAULT_INDEX,
} from "../StoryVO";

function isEqualObjects(a, b) {
  // Only works for the particular use case here - a and b must be objects!
  if (
    Object.getOwnPropertyNames(a).length ===
    Object.getOwnPropertyNames(b).length
  ) {
    let propName;
    for (propName in a) {
      if (a[propName] !== b[propName]) {
        return false;
      }
    }
  } else {
    return false;
  }
  return true;
}

const story1 = {
  id: "#1adj8dgfum1",
  parentId: "#1564m7s2jsl",
  title: "Test story",
  body: "This a test story #urgent",
  index: 0,
};

const storyWithTypeErrors = {
  id: "#1adj8dgfum1",
  parentId: 6,
  title: true,
  body: false,
  index: "0",
};

describe("It initialises and updates correctly", () => {
  it("returns the correct className", () => {
    expect(new StoryVO({}).className).toBe("StoryVO");
  });

  it("takes valid story data and returns the object back", () => {
    const story = new StoryVO(story1);
    expect(isEqualObjects(story1, story)).toBe(true);
  });

  it("takes and incomplete object and returns a full VO", () => {
    const story = new StoryVO({});
    expect(story.id).toBeTruthy();
    expect(story.parentId).toBe(DEFAULT_PARENT_ID);
    expect(story.title).toBe(DEFAULT_TITLE);
    expect(story.body).toBe(DEFAULT_BODY);
    expect(story.index).toBe(DEFAULT_INDEX);
  });

  it("is uneditable when change is attempted directly", () => {
    const story = new StoryVO({});
    let errorMsg;
    try {
      story.title = "some other story";
    } catch (e) {
      errorMsg = e.name;
    }
    // The title should not change.
    expect(errorMsg).toBe("TypeError");
    expect(story.title).toBe(DEFAULT_TITLE);
  });

  it("creates a new instance on 'change' because it is immutable", () => {
    let story = new StoryVO(story1);
    let storyRef = story;
    story = StoryVO.update(story, { title: "Some new title" });
    // story will be changed, but the ref will still point to the old version.
    expect(story.title).toBe("Some new title");
    expect(storyRef.title).toBe("Test story");
  });
});

describe("it performs run time type checking on change", () => {
  it("throws on badly typed initialization data", () => {
    let msg;
    try {
      new StoryVO(storyWithTypeErrors);
    } catch (e) {
      msg = e.message;
    }
    expect(msg.includes("StoryVO")).toBe(true);
    expect(msg.includes("parentId should be null or a string")).toBe(true);
    expect(msg.includes("title should be a string")).toBe(true);
    expect(msg.includes("body should be a string")).toBe(true);
    expect(msg.includes("index should be a number")).toBe(true);
  });

  it("throws on bad change data", () => {
    let story = new StoryVO(story1);
    let msg;

    try {
      story = StoryVO.update(story, { title: 0 });
    } catch (e) {
      msg = e.message;
    }
    expect(msg).toBe("StoryVO - title should be a string.");
  });
});

describe("it can be forced to ignore type checking", () => {
  it("will ignore type checking on initialization when forced", () => {
    const story = new StoryVO(storyWithTypeErrors, false);
    expect(isEqualObjects(story, storyWithTypeErrors)).toBe(true);
  });

  it("will ignore type checking on updates when forced", () => {
    let story = new StoryVO(story1);
    story = StoryVO.update(story, { title: 123 }, false);
    expect(story.title).toBe(123);
  });
});

describe("it always returns a valid id", () => {
  it("creates an id if none is provided", () => {
    const story = new StoryVO({});
    expect(typeof story.id).toBe("string");
    expect(story.id.length >= MINIMUM_ID_LENGTH).toBe(true);
  });

  it("raises an error if the id is unsuitable", () => {
    let msg;

    try {
      new StoryVO({ id: 3 });
    } catch (e) {
      msg = e.message;
    }
    expect(msg).toBe("StoryVO - (BaseVO) id is too short.");
  });

  it("returns an object with the provided id if the id is valid", () => {
    const newId = "#1adj8dgfum1";
    const story = new StoryVO({ id: newId });
    expect(story.id).toBe(newId);
  });
});
