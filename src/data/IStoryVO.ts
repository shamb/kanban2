/* istanbul ignore file */
export default interface IStoryVO {
  className:string;
  id: string,
  parentId: string,
  title: string,
  body: string,
  index: number,
}

// minimum properties a raw object used to initialise a IStoryVO must have.
export type StoryInitialiser = {
  id: string | null,
  parentId: string,
  title: string,
  body: string,
  index: number,
}




