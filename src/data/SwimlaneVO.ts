import { getId } from "../helpers/utils";
import { USE_RUNTIME_TYPING, MINIMUM_ID_LENGTH } from "./voConstants";
import BaseVO from "./BaseVO";
// Interfaces
import ISwimlaneVO, { SwimlaneInitialiser } from "./ISwimlaneVO";

const DEFAULT_TITLE: string = "New Swimlane";
const DEFAULT_INDEX: number = 0;
const defaultObj: SwimlaneInitialiser = {
  id: null,
  title: DEFAULT_TITLE,
  index: DEFAULT_INDEX,
};

// The Swimlane Value Object.

class SwimlaneVO extends BaseVO implements ISwimlaneVO {
  private static readonly _className:string = "SwimlaneVO";
  //
  // public id:string (inherited from BaseVO)
  public title: string;
  public index: number;

  constructor(
    { id,
      title = defaultObj.title,
      index = defaultObj.index
    }: SwimlaneInitialiser = defaultObj, checkTypes: boolean = USE_RUNTIME_TYPING) {
    super({ id });
    this.title = title;
    this.index = index;
    if (checkTypes) {
      SwimlaneVO.checkVOForTypes(this);
    }
    Object.freeze(this);
  }

  get className(): string {
     // NB - we can't use this.constructor.name because it can be lost in production (bundlers).
    return SwimlaneVO._className;
  }

  public static update(instance: ISwimlaneVO, changes: any, checkTypes: boolean = USE_RUNTIME_TYPING): ISwimlaneVO {
    const updatedData: any = { ...instance, ...changes };
    return new SwimlaneVO(updatedData, checkTypes);
  }

  public static checkVOForTypes(obj: any): void {
    const { title, index } = obj;
    let errors: string[] = [...super.checkBaseData(obj)];

    // NB - id cannot be changed after creation.
    if (typeof title !== "string") {
      errors.push("title should be a string");
    }
    if (typeof index !== "number") {
      errors.push("index should be a number");
    }
    if (errors.length > 0) {
      throw new TypeError(`SwimlaneVO - ${errors.join(", ")}.`);
    }
  }

}


export default SwimlaneVO;

// these exports are for testing.
export { DEFAULT_TITLE, DEFAULT_INDEX };
