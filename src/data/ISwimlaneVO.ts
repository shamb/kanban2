/* istanbul ignore file */
export default interface ISwimlaneVO {
  className:string;
  id:string,
  title:string,
  index:number,
}

// minimum properties a raw object used to initialise an ISwimlaneVO must have.
export type SwimlaneInitialiser = {
  id: string | null,
  title: string,
  index: number;
}
