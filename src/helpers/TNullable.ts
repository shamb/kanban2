/* istanbul ignore file */
type Nullable<T> = T | undefined | null;

export default Nullable;
