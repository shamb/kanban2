/* istanbul ignore file */
// Associative array. Can be initialised as either [] or {}.
type AssociativeArray = {
  [key: string]: any;
}

export default AssociativeArray;