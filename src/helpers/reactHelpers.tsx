/* istanbul ignore file */
// Not tested as we're testing via testing library, and this is internal.

import { useRef, useEffect } from 'react';

function useTimeout(callback: Function, delay: number | null = null, isPaused: boolean = false) {
  let delayedFunction = useRef<Function | undefined>();
  let timeoutId = useRef<number | undefined>();

  useEffect(() => {
    // Remember the latest callback.
    delayedFunction.current = callback;
  }, [callback]);

  // Set up the interval.
  useEffect(() => {
    function tick() {
      if (delayedFunction.current) {
        delayedFunction.current();
      }
    }
    if (delay !== null) {
      if (!isPaused) {
        // Set the timeout.
        timeoutId.current = window.setTimeout(tick, delay);
      } else {
        // clear the timeout.
        window.clearTimeout(timeoutId.current);
      }
    }
  }, [delay, isPaused]);
}

export { useTimeout };