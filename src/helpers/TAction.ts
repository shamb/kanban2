/* istanbul ignore file */
// Returned from /helpers/utils > getNewAction.

export type Action = {
  type: string,
  data?: any,
  forHumans?: string,
}