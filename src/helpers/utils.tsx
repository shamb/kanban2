
/* istanbul ignore file */
// Not tested as we're testing via testing library, and this is internal.

// Interfaces
import { Action } from "./TAction";

const bigMax: number = Number.MAX_SAFE_INTEGER;
const bigMin: number = 1000000000000000;
const bigRange: number = bigMax - bigMin;
const OBJECT: string = "object";

// returns a random string pattern of the form # then several lower case chars.
// Use this to create random compact ids or keys for React.
// NB - this is not a RFC uuid.
const getId = (): string =>
  `#${Math.floor(Math.random() * bigRange + bigMin).toString(36)}`;

// Creates a new action (for reducers etc).
const getNewAction = (type: string = "", data: any = {}, forHumans: string = ""): Action => ({
  type,
  data,
  forHumans
});

// Create a deep clone.
const deepClone = (obj: any) => {
  let out: any;
  let val: any;
  let key: string;
  if (typeof obj !== OBJECT) {
    // It is a Boolean, string or number.
    out = obj;
  } else {
    // It is an array or object
    out = Array.isArray(obj) ? [] : {};
    for (key in obj) {
      val = obj[key];
      out[key] = typeof val === OBJECT ? deepClone(val) : val;
    }
  }
  return out;
};

export { getId, getNewAction, deepClone };
