// Framework libs
import React, { useEffect, useState, useReducer } from "react";
import PropTypes, {InferProps} from "prop-types";
// Application - view
import Toasts from "./components/toast/Toasts";
import Header from "./components/header/Header";
import Swimlanes from "./components/swimlanes/Swimlanes";
// Application - data layer
import { setDispatchers } from "./reducers/dispatchers";
import { toastReducer, ADD_TOAST } from "./reducers/toastReducer";
import ToastVO from "./data/ToastVO";
import { swimlanesReducer, ADD_SWIMLANES } from "./reducers/swimlanesReducer";
import { storiesReducer, ADD_STORIES } from "./reducers/storiesReducer";
import { statusReducer } from "./reducers/statusReducer";
import { boardTitleReducer, CHANGE_TITLE } from "./reducers/boardTitleReducer";
import { getNewAction } from "./helpers/utils";
// Interfaces
import Status from "./reducers/TStatus";
import { AppData } from "./TAppData";
import { SwimlaneInitialiser } from "./data/ISwimlaneVO";
import { StoryInitialiser } from "./data/IStoryVO";
import { ToastInitialiser } from "./data/IToastVO";
// Styling
import styles from "./App.module.css";

const DEFAULT_STATUS: Status = {
  isEditMode: false
};

type AppTypes = InferProps<typeof App.propTypes>;

function App(props:AppTypes) {
  // Global state
  const { url } = props;
  const [stories, dispatchStories] = useReducer(storiesReducer, []);
  const [swimlanes, dispatchSwimlanes] = useReducer(swimlanesReducer, []);
  const [toasts, dispatchToasts] = useReducer(toastReducer, []);
  const [status, dispatchStatus] = useReducer(statusReducer, DEFAULT_STATUS);
  const [boardTitle, dispatchBoardTitle] = useReducer(boardTitleReducer, "");
  const [searchTerm, setSearchTerm] = useState("");
  const [boardId, setBoardId] = useState<string>("");
  // local state
  const [isReady, setIsReady] = useState(false);

  useEffect(() => {
    // Runs on component mount

    let isCancelled = false;

    async function dataFetcher(url: string): Promise<AppData | null> {
      const response: Response = await fetch(url);
      const data: AppData = await response.json();
      //console.log(data);
      if (!isCancelled) {
        return data;
      } else {
        return null;
      }
    };

    function onDataLoaded(data: AppData): void {
      let { boardTitle, swimlanes, stories, id } = data;
      // Sort the swimlanes into their index order, then sort the stories into
      // swimlane and index order. We should end up with the swimlanes in the
      // order they will be displayed left-right, and the stories in the order
      // they will be displayed top left to bottom right. We do this for
      // simplicity; the logic is a lot easier given we can assume this order.
      stories = sortStories(swimlanes, stories);

      dispatchStories(
        getNewAction(
          ADD_STORIES,
          stories,
          "Adding stories to state on start-up."
        )
      );
      dispatchSwimlanes(
        getNewAction(
          ADD_SWIMLANES,
          swimlanes,
          "Adding swimlanes to state on start-up."
        )
      );
      dispatchBoardTitle(
        getNewAction(CHANGE_TITLE, { boardTitle }, "Setting board title")
      );
      setBoardId(id);
      setIsReady(true);
      dispatchToasts(
        getNewAction(ADD_TOAST, {
          message: "Ready.",
          duration: 1000,
          type: ToastVO.INFO,
          level: ToastVO.TOAST_LEVEL_DEBUG
        } as ToastInitialiser)
      );
    }

    setDispatchers({
      dispatchStories,
      dispatchSwimlanes,
      dispatchToasts,
      dispatchStatus,
      dispatchBoardTitle,
      setSearchTerm,
    });

    dataFetcher(url)
      .then((data: AppData | null):void => {
        if (data !== null) {
          onDataLoaded(data);
        }
      });

      return ():void => {
        // cleanup on unmount
        isCancelled = true;
      }
  }, [url]);

  return (
    <>
      {!isReady && <p>Loading...</p>}
      {isReady && (
        <div className={styles.app} data-testid="app">
          <Toasts toasts={toasts} />
          <div className={styles.board} data-testid="board">
            <Header boardTitle={boardTitle} isEditMode={status.isEditMode} />
            <Swimlanes
              swimlanes={swimlanes}
              stories={stories}
              filterString={searchTerm}
              isEditMode={status.isEditMode}
            />
          </div>
        </div>
      )}
    </>
  );
}

function sortStories(swimlanes: SwimlaneInitialiser[], stories: StoryInitialiser[]): StoryInitialiser[] {
  let swimlaneStories: any[] = [];
  let sortedStories: StoryInitialiser[] = [];

  // sort the stories into swimlanes.
  swimlaneStories = swimlanes.map((swimlane: any) => ({
    id: swimlane.id,
    stories: []
  }));
  stories.forEach((story: any) => {
    const parentId: string = story.parentId;
    swimlaneStories.some(swimlane => {
      const isFound: boolean = parentId === swimlane.id;
      if (isFound) {
        swimlane.stories.push(story);
      }
      return isFound;
    });
  });
  // sort the stories by swimlane then index
  swimlaneStories.forEach((swimlane: any) => {
    swimlane.stories = swimlane.stories.sort((a: any, b: any) =>
      a.index > b.index ? 1 : -1
    );
    sortedStories = sortedStories.concat(swimlane.stories);
  });
  // return the sorted stories
  return sortedStories;
}

App.propTypes = {
  url:PropTypes.string.isRequired,
};

export default App;
