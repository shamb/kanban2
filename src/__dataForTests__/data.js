/* istanbul ignore file */
const testData = {
  "boardTitle":"new kanban",
  "id": "#20gxtbe9sdb",
  "stories": [
    { 
      "id":"#1adj8dgfum1",
      "parentId":"#1564m7s2jsl", 
      "title": "Test story", 
      "body":"This a test story #shambhangal #urgent",
      "index":0
     },
    {
      "id": "#22bnkb62nl7", 
      "parentId":"#1564m7s2jsl",
      "title": "Another test story", 
      "body": "This is another test story #shambhangal #urgent",
      "index":1
    },
    {
      "id":"#1hppt7he31l",
      "parentId":"#227btyu14ex", 
      "title":"Story in progress", 
      "body":"This a test story #shambhangal",
      "index":0
    },
    {
      "id":"#ch8o1g8rh2",
      "parentId":"#227btyu14ex", 
      "title":"Story in progress", 
      "body":"This a another test  story #shambhangal",
      "index":1
    },
    {
      "id":"#pebhva79q2",
      "parentId":"#20pgb3y1l2g", 
      "title":"Story is done", 
      "body":"This a test story #shambhangal",
      "index":0
    }

  ],
  "swimlanes": [
    {
      "title":"Backlog",
      "id":"#1564m7s2jsl",
      "index":0
    },
    {
      "title":"Ready",
      "id":"#1075qkb00o4",
      "index":1
    },
    {
      "title":"In Progress",
      "id":"#227btyu14ex",
      "index":2
    },
    {
      "title":"In Test",
      "id":"#1cwrj8lbiyv",
      "index":3
    },
    {
      "title":"Done",
      "id":"#20pgb3y1l2g",
      "index":4
    }
  ]
}

export default testData;