/* istanbul ignore file */
// Interfaces 
import { SwimlaneInitialiser }  from "./data/ISwimlaneVO";
import { StoryInitialiser } from "./data/IStoryVO";

// type used to hold initial app data object.

export type AppData = {
  boardTitle:string,
  swimlanes:SwimlaneInitialiser[], 
  stories:StoryInitialiser[], 
  id:string,
}