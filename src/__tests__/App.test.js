import React from "react";
import App from "../App";
import { rest } from 'msw'
import {setupServer } from "msw/node";
import { toBeInTheDocument, } from '@testing-library/jest-dom';
import { render, waitFor, within, cleanup, screen } from '@testing-library/react';

import testData from "../__dataForTests__/data";


const server = setupServer(
  rest.get('/testData.json', (req, res, ctx) => {
    const returnData = res(ctx.json(testData))
    return returnData;
  })
)

describe("Top level layout: loading state", () => {
  beforeEach(() => {
    render(<App url="testData.json" />);
  });
  afterEach(() => {
    cleanup();
  })

  test("it renders the loading screen", () => {
    expect(screen.getByText("Loading...")).toBeInTheDocument();
  });
  
  test("it doesn't render the main ui during load", () => {
    // NB - use queryBy instead of getBy to avoid an exception on 'not present'!
    expect(screen.queryByTestId("board")).toBeFalsy();
  });

});


describe("top level layout: initial loaded state", () => {
  beforeEach(() => server.listen());
  afterEach(() => server.resetHandlers());
  afterAll(() => server.close());
  beforeEach( async () => {
    render(<App url="testData.json" />);
    await waitFor(() => {
      expect(screen.getByTestId("app"));
    });
  });
  
  test("it renders the main containers (app, toastList and main board)", async () => {
    expect(screen.getByTestId("app")).toBeInTheDocument();
    expect(screen.getByTestId("toastList")).toBeInTheDocument();
    expect(screen.getByTestId("board")).toBeInTheDocument();
  });
  
  test ("it renders the correct board title", async () => {
    const boardTitle = testData.boardTitle;
    expect(screen.getByText(boardTitle)).toBeInTheDocument();
  });
  
  test ("it renders the correct number of swimlanes", async () => {
    const expectedCount = testData.swimlanes.length;
    expect(screen.getAllByTestId("swimlane").length).toEqual(expectedCount);

  });
  
  test("it renders the correct number of stories", async () => {
    const expectedCount = testData.stories.length;
    expect(screen.getAllByTestId("storyCard").length).toEqual(expectedCount);  
  });
  
  
  test("it renders a 'Ready.' toast message on start-up", () => {
    const toast = screen.getByTestId("toast");
    const theToast = within(toast);
    expect(theToast.getByText("Ready.")).toBeInTheDocument();
  });
  
});

