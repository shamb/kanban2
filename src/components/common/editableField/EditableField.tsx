import React, {
  useState,
  useRef,
  useEffect,
  SyntheticEvent,
  ReactElement,
} from "react";
import PropTypes, { InferProps } from "prop-types";

import styles from "./EditableField.module.css";

// Text that appears as static non-editable text until it's isEditMode
// property is true, whereupon it becomes an editable text field.

type EditableFieldProps = InferProps<typeof EditableField.propTypes>;

function EditableField(props: EditableFieldProps): ReactElement {
  const { text, isEditMode, validationRegex, onChange, width } = props;
  const [fieldText, setFieldText] = useState<string>(text);
  const prevEditMode = useRef<boolean>(isEditMode);
  const uneditedValue = useRef<string>(text);
  const isValid = useRef<boolean>(true);
  const style = { minWidth: width || "initial" };
  let content: any;

  useEffect(() => {
    // Get previous edit mode (so we can detect
    // editor open/close transitions).
    prevEditMode.current = isEditMode;
  });

  useEffect(() => {
    // Put good value updates back into local state.
    setFieldText(text);
  }, [text]);

  const onChangeEvt = (e: SyntheticEvent): void => {
    const changedText: string = (e.target as HTMLInputElement).value;
    isValid.current = new RegExp(validationRegex).test(changedText);
    setFieldText(changedText);
  };

  if (isEditMode) {
    const hasJustOpenedEditor: boolean = !prevEditMode.current;
    if (hasJustOpenedEditor) {
      uneditedValue.current = text;
    }
    let className = isValid.current
      ? `${styles.editableText} ${styles.valid}`
      : `${styles.editableText} ${styles.invalid}`;
    content = (
      <input
        className={className}
        data-testid="editableText"
        type="text"
        value={fieldText}
        onChange={onChangeEvt}
        style={style}
      ></input>
    );
  } else {
    const hasJustClosedEditor: boolean = prevEditMode.current;
    const hasValueChanged: boolean = uneditedValue.current !== fieldText;
    if (hasJustClosedEditor && isValid.current && hasValueChanged) {
      // successful edit; bring unedited value up to date and send out edit.
      uneditedValue.current = fieldText;
      onChange(fieldText);
    } else if (hasJustClosedEditor && !isValid.current) {
      // unsuccessful edit; revert to last good values and don't sent out edit.
      if (fieldText !== uneditedValue.current) {
        setFieldText(uneditedValue.current);
        isValid.current = true;
      }
    }
    content = (
      <div
        className={`${styles.editableText} ${styles.noEdit}`}
        data-testid="nonEditableText"
        style={style}
      >
        {fieldText}
      </div>
    );
  }
  return content;
}

EditableField.propTypes = {
  text: PropTypes.string.isRequired,
  isEditMode: PropTypes.bool.isRequired,
  validationRegex: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.instanceOf(RegExp),
  ]).isRequired,
  validationErrorText: PropTypes.string,
  onChange: PropTypes.func.isRequired,
  width: PropTypes.string,
};

//TODO: implement validationErrorText in the code (it is defined as a prop but not used).

export default EditableField;
