import React from "react";
import EditableField from "../EditableField";
import { toBeInTheDocument } from "@testing-library/jest-dom";
import {
  render,
  cleanup,
  screen,
  fireEvent,
} from "@testing-library/react";

describe("It renders correctly depending on the isEditMode prop", () => {
  const testText = "Some text.";
  afterEach(() => {
    cleanup();
  });

  it("shows a non editable or editable field depending on edit mode ", () => {
    const handler = jest.fn();
    //
    // start with a non-editable field.
    const { rerender } = render(
      <EditableField
        text={testText}
        isEditMode={false}
        validationRegex="^([a-zA-Z0-9 ]){10,30}$"
        onChange={handler}
      />
    );
    expect(screen.getByText(testText)).toBeInTheDocument();
    expect(screen.getByTestId("nonEditableText")).toBeInTheDocument();
    //
    //change isEditMode to true and show we now see a editable field.
    rerender(
      <EditableField
        text={testText}
        isEditMode={true}
        validationRegex="^([a-zA-Z0-9 ]){10,30}$"
        onChange={handler}
      />
    );
    expect(screen.getByDisplayValue(testText)).toBeInTheDocument();
    expect(screen.getByTestId("editableText")).toBeInTheDocument();
    //
    // Change back to a non editable field to complete the from-to state transitions.
    rerender(
      <EditableField
        text={testText}
        isEditMode={false}
        validationRegex="^([a-zA-Z0-9 ]){10,30}$"
        onChange={handler}
      />
    );
    expect(screen.getByText(testText)).toBeInTheDocument();
    expect(screen.getByTestId("nonEditableText")).toBeInTheDocument();
  });
});

describe("it correctly handles input validity during edits", () => {
  afterEach(() => {
    cleanup();
  });

  it("initially shows a green field (valid text) but this turns to red for invalid text", () => {
    render(
      <EditableField
        text="Some long text"
        isEditMode={true}
        validationRegex="^([a-zA-Z0-9 ]){10,30}$"
        onChange={jest.fn()}
      />
    );
    const field = screen.getByTestId("editableText");
    // Find valid text...
    expect(field.classList.contains("valid")).toBeTruthy();
    //
    // Then simulate the user entering invalid text (string too short)...
    fireEvent.change(field, { target: { value: "Too short" } });
    expect(field.classList.contains("invalid")).toBeTruthy();
    //
    // Now make it valid again to complete the from-to state transitions...
    fireEvent.change(field, { target: { value: "Text is valid again" } });
    expect(field.classList.contains("valid")).toBeTruthy();
  });
});

describe("correctly implements change and validation on the user ending edit", () => {
  afterEach(() => {
    cleanup();
  });

  it("correctly updates data and view to the last entered value when a valid text edit ends", () => {
    const userEditValue = "Some valid text"
    let strValue = "unchanged";
    let field;
    const handler = jest.fn((str) => {
      strValue=str;
    });
    // Start with a non-editable field...
    const { rerender } = render(
      <EditableField
        text={strValue}
        isEditMode={false}
        validationRegex="^([a-zA-Z0-9 ]){4,30}$"
        onChange={handler}
      />
    );
    //field = screen.getByTestId("nonEditableText")
    //screen.debug(field);
    // Set it to editable...
    rerender(
      <EditableField
        text={strValue}
        isEditMode={true}
        validationRegex="^([a-zA-Z0-9 ]){4,30}$"
        onChange={handler}
      />
    );
    // Simulate the user changing to the field to a valid value,
    // then close the editable field.
    field = screen.getByTestId("editableText")
    //screen.debug(field);
    fireEvent.change(field, { target: { value: userEditValue } });
    rerender(
      <EditableField
      text={strValue}
      isEditMode={false}
      validationRegex="^([a-zA-Z0-9 ]){4,30}$"
      onChange={handler}
      />
    );
    //field = screen.getByTestId("nonEditableText")
    //screen.debug(field);
    field = screen.getByTestId("nonEditableText");
    expect(strValue).toBe(userEditValue);
    expect(field.textContent).toBe(userEditValue);
    // NB - we don't need to test whether the callback 'handler' ran
    // because it has to have run for strValue to have changed.
  });

  it("correctly resets data and view to the last entered value when an invalid text edit ends", () => {
    const userEditValue = "Some invalid text..."; // the '...' is not valid
    const originalText ="unchanged"
    let strValue = originalText;
    let field;
    const handler = jest.fn((str) => {
      strValue=str;
    });
    // Start with a non-editable field...
    const { rerender } = render(
      <EditableField
        text={strValue}
        isEditMode={false}
        validationRegex="^([a-zA-Z0-9 ]){4,30}$"
        onChange={handler}
      />
    );
    // Set it to editable...
    rerender(
      <EditableField
        text={strValue}
        isEditMode={true}
        validationRegex="^([a-zA-Z0-9 ]){4,30}$"
        onChange={handler}
      />
    );
    // Simulate the user changing to the field to a valid value,
    // then close the editable field.
    field = screen.getByTestId("editableText")
    fireEvent.change(field, { target: { value: userEditValue } });
    rerender(
      <EditableField
      text={strValue}
      isEditMode={false}
      validationRegex="^([a-zA-Z0-9 ]){4,30}$"
      onChange={handler}
      />
    );
    // Show that the field reverts to the previous valid value.
    field = screen.getByTestId("nonEditableText");
    expect(strValue).toBe(originalText);
    expect(field.textContent).toBe(originalText);
  });

});
