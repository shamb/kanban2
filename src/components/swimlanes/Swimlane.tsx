// Framework libs
import React, { useRef, useState, useEffect, ReactElement, ReactNode } from "react";
import PropTypes, { InferProps } from "prop-types";
import { Droppable, DroppableProvided } from "react-beautiful-dnd";
// 3rd party components
import { Tooltip, Whisper } from "rsuite";
// Application
import Story from "../story/Story";
import EditableField from "../common/editableField/EditableField";
import { getDispatchers } from "../../reducers/dispatchers";
import { getNewAction } from "../../helpers/utils";
import { EDIT_SWIMLANE } from "../../reducers/swimlanesReducer";
// Interfaces and types
import { Action } from "../../helpers/TAction";
// Styling
import styles from "./Swimlane.module.css"

// A Kanban swimlane column. Setting the isEditMode to true makes 
// the swimlane editable. When false, the swimlane shows its
// stories.

type SwimlaneProps = InferProps<typeof Swimlane.propTypes>

function Swimlane(props: SwimlaneProps): ReactElement {
  const {
    id,
    stories,
    title,
    isEditMode,
    filterString,
    isFirst,
    isLast,
    onAddSwimlane,
    onRemoveSwimlane,
    useAnimation
  } = props;
  const [isBeingDeleted, setIsBeingDeleted] = useState(false);
  const initialUseAnimation = useRef(useAnimation);
  const dispatchSwimlanes = useRef<Function>();
  const isEmpty: boolean = stories.length === 0;
  const canDeleteSwimlane: boolean = !isFirst && !isLast;
  const isEditableDeletableAndEmpty: boolean =
    isEditMode && canDeleteSwimlane && isEmpty;
  const isEditableDeletableAndNotEmpty: boolean =
    isEditMode && canDeleteSwimlane && !isEmpty;
  const canAddSwimlane: boolean = !isLast;
  const bodyClass: string =
    isEditMode && !isEmpty ? `${styles.swimlaneBody} ${styles.disabled}` : styles.swimlaneBody;
  let containerClass: string;
  let storiesView: ReactNode;

  if (isBeingDeleted) {
    // This swimlane is being deleted.
    containerClass = `${styles.swimlane} ${styles.reverseAnimation}`;
  } else {
    // This swimlane has been created after application start or has existed
    // since the start of the application.
    containerClass = initialUseAnimation.current
      ? styles.swimlane
      : `${styles.swimlane} ${styles.noAnimation}`;
  }

  const delayedDelete = (): void => {
    setIsBeingDeleted(true);
    // Allow time for the remove animation to fire before
    // we send the remove swimlane action.
    setTimeout(() => {
      onRemoveSwimlane(id);
    }, 200);
  };

  const onTitleChange = (changedTitle: string): void => {
    console.log("swimlane title change");
    const action: Action = getNewAction(
      EDIT_SWIMLANE,
      { id, change: { title: changedTitle } },
      `Changed title of swimlane ${id} from ${title} to ${changedTitle}`
    );
    dispatchSwimlanes.current!(action)
  }

  useEffect(() => {
    // Get dispatchers on mount.
    const dispatchers: any = getDispatchers();
    dispatchSwimlanes.current = dispatchers.dispatchSwimlanes;
  }, []);

  storiesView = stories.map((story, index): ReactNode => (
    <Story
      id={story.id}
      index={index}
      isFiltered={`${story.title}${story.body}`.includes(filterString)}
      key={story.id}
      title={story.title}
      body={story.body}
    />
  ));

  return (
    <div id={id} className={containerClass} data-testid="swimlane">
      {isEditableDeletableAndEmpty && !isBeingDeleted && (
        <Whisper
          placement="right"
          trigger="hover"
          delayShow={200}
          speaker={<Tooltip>Delete this swimlane</Tooltip>}
        >
          <img className={styles.deleteIcon} data-testid="deleteSwimlaneIcon" alt="" onClick={() => delayedDelete()} />
        </Whisper>
      )}
      {isEditableDeletableAndNotEmpty && !isBeingDeleted && (
        <Whisper
          className="tooltip-warning"
          placement="right"
          trigger="hover"
          speaker={<Tooltip>Cannot delete a swimlane with stories</Tooltip>}
        >
          <img className={`${styles.deleteIcon} ${styles.disabled}`} data-testid="deleteSwimlaneIconDisabled" alt="" />
        </Whisper>
      )}
      {isEditMode && canAddSwimlane && !isBeingDeleted && (
        <Whisper
          placement="right"
          trigger="hover"
          delayShow={500}
          speaker={<Tooltip>Insert a swimlane</Tooltip>}
        >
          <img className={styles.addIcon} data-testid="insertSwimlane" alt="" onClick={() => onAddSwimlane(id)} />
        </Whisper>
      )}
      <div className={styles.swimlaneHead} data-testid="swimlaneHead">
        <EditableField
          text={title}
          isEditMode={isEditMode}
          validationRegex="^([a-zA-Z0-9 ]){4,30}$"
          validationErrorText="Title must be between 4 and 30 alphanumeric characters"
          onChange={onTitleChange}
          width="230px"
        />
      </div>
      <Droppable droppableId={id}>
        {(provided: DroppableProvided): ReactElement => (
          <div
            className={bodyClass} data-testid="swimlaneBody"
            ref={provided.innerRef}
            {...provided.droppableProps}
          >
            {storiesView}
            {provided.placeholder}
          </div>
        )}
      </Droppable>
    </div>
  );
}

Swimlane.propTypes = {
  id: PropTypes.string.isRequired,
  stories: PropTypes.array.isRequired,
  title: PropTypes.string.isRequired,
  isEditMode: PropTypes.bool.isRequired,
  filterString: PropTypes.string.isRequired,
  isFirst: PropTypes.bool.isRequired,
  isLast: PropTypes.bool.isRequired,
  onAddSwimlane: PropTypes.func.isRequired,
  onRemoveSwimlane: PropTypes.func.isRequired,
  useAnimation: PropTypes.bool.isRequired
};

export default Swimlane;
