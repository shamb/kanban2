// Framework libs
import React, { ReactElement, ReactNode, useEffect, useRef, useState } from "react";
import PropTypes from "prop-types";
import { DragDropContext, DropResult } from "react-beautiful-dnd";
// Application
import { ADD_SWIMLANE, REMOVE_SWIMLANE } from "../../reducers/swimlanesReducer";
import { MOVE_STORY } from "../../reducers/storiesReducer";
import { getDispatchers } from "../../reducers/dispatchers";
import { getNewAction } from "../../helpers/utils";
import Swimlane from "./Swimlane";
// Data
import SwimlaneVO from "../../data/SwimlaneVO";
import StoryVO from "../../data/StoryVO";
// Interfaces and types
import { Action }  from "../../helpers/TAction";
import ISwimlaneVO from "../../data/ISwimlaneVO";
import IStoryVO from "../../data/IStoryVO";
// Styling
import styles from "./Swimlanes.module.css";

// We can't use inferProps, as that will assume swimlanes and stories can be
// arrays of nulls or undefined.
// type SwimlaneProps = InferProps<typeof Swimlanes.propTypes>;
type SwimlanesProps = {
  swimlanes:ISwimlaneVO[],
  stories:IStoryVO[],
  isEditMode:boolean,
  filterString:string,
}; 


function Swimlanes(props:SwimlanesProps):ReactElement {
  const { swimlanes, stories, isEditMode, filterString } = props;
  const [isFirstRender, setIsFirstRender] = useState(true);
  const dispatchSwimlanes = useRef<Function>();
  const dispatchStories = useRef<Function>();
  const lastSwimlaneIndex:number = swimlanes.length - 1;
  let swimlanesView:ReactNode;

  useEffect(() => {
    // Get dispatchers on mount.
    dispatchSwimlanes.current = getDispatchers().dispatchSwimlanes;
    dispatchStories.current = getDispatchers().dispatchStories;
    setIsFirstRender(false);
  }, []);

  const onAddSwimlane = (id:string):void => {
    const action:Action = getNewAction(ADD_SWIMLANE, { addAfterId: id }, "Adding a swimlane");
    // Add a swimlane after the swimlane with id.
    console.log(`onAddSwimlane after ${id}`);
    dispatchSwimlanes.current!(action);
  };

  const onRemoveSwimlane = (id:string):void => {
    const action:Action = getNewAction(REMOVE_SWIMLANE, { id }, "Removing a swimlane");
    // delete the swimlane with id.
    console.log(`onRemoveSwimlane ${id}`);
    dispatchSwimlanes.current!(action);
  };
  swimlanesView = swimlanes.map((swimlane:SwimlaneVO, index:number):ReactNode => {
    const { title, id } = swimlane;
    const swimlaneStories:IStoryVO[] = stories.filter((story:IStoryVO) => story.parentId === id);
    const isFirst:boolean = index === 0;
    const isLast:boolean = index === lastSwimlaneIndex;
    return (
      <Swimlane
        key={id}
        id={id}
        title={title}
        stories={swimlaneStories}
        isEditMode={isEditMode}
        filterString={filterString}
        isFirst={isFirst}
        isLast={isLast}
        onAddSwimlane={onAddSwimlane}
        onRemoveSwimlane={onRemoveSwimlane}
        useAnimation={!isFirstRender}
      />
    );
  });

  const onDragEnd = (response:DropResult):void => {
    const { source, destination } = response;
    const storyId = response.draggableId;
    
    if (destination !== undefined && destination !== null) {
      console.log("dragEnd");
      const from = {
        storyId,
        storyIndex: source.index,
        swimlaneId: source.droppableId
      };
      const to = {
        storyId,
        storyIndex: destination.index,
        swimlaneId: destination.droppableId
      };
      const swimlaneList = props.swimlanes.map((swimlane:ISwimlaneVO):string => swimlane.id);
      const action:Action = getNewAction(
        MOVE_STORY,
        { from, to, swimlaneList },
        `Move story ${storyId} from swimlane ${from.swimlaneId} position ${from.storyIndex} to ${to.swimlaneId} position ${to.storyIndex}`
      );
      dispatchStories.current!(action);
    }
  }

  return (
    <DragDropContext onDragEnd={onDragEnd}>
      <div className={styles.swimlanes} data-testid="swimlanes">{swimlanesView}</div>
    </DragDropContext>
  );
}

Swimlanes.propTypes = {
  swimlanes: PropTypes.arrayOf(PropTypes.instanceOf(SwimlaneVO)).isRequired,
  stories: PropTypes.arrayOf(PropTypes.instanceOf(StoryVO)).isRequired,
  isEditMode: PropTypes.bool.isRequired,
  filterString: PropTypes.string.isRequired,
};

export default Swimlanes;
