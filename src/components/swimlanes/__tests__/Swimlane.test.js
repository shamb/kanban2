import React from "react";
import { toBeInTheDocument } from "@testing-library/jest-dom";
import {
  getByText,
  render,
  screen,
  fireEvent,
  getAllByTestId,
  within,
  cleanup,
} from "@testing-library/react";
import { DragDropContext } from "react-beautiful-dnd";

import data from "../../../__dataForTests__/data";
import SwimlaneVO from "../../../data/SwimlaneVO";
import StoryVO from "../../../data/StoryVO";
import { EDIT_SWIMLANE } from "../../../reducers/swimlanesReducer";

import Swimlane from "../Swimlane";

function makeTestData(data) {
  const swimlane = new SwimlaneVO(data.swimlanes[0]);
  const stories = data.stories
    .filter((story) => story.parentId === swimlane.id)
    .map((story) => new StoryVO(story));
  const emptyStories = [];

  return { swimlane, stories, emptyStories };
}

jest.mock("../../../reducers/dispatchers", () => {
  return {
    setDispatchers: () => {},
    getDispatchers: () => {
      return {
        dispatchSwimlanes: (action) => {
          swimlanesFired = action;
        },
      };
    },
  };
});
function resetDispatcherMock() {
  swimlanesFired = { type: "notFired" };
}

let swimlanesFired;

describe("it renders correctly in view mode when there are stories", () => {
  let testData = makeTestData(data);
  let swimlane = testData.swimlane;
  let stories = testData.stories;

  beforeEach(() => {
    testData = makeTestData(data);
    swimlane = testData.swimlane;
    stories = testData.stories;
    render(
      <DragDropContext onDragEnd={jest.fn()}>
        <Swimlane
          key={swimlane.id}
          id={swimlane.id}
          title={swimlane.title}
          stories={stories}
          isEditMode={false}
          filterString={""}
          isFirst={false}
          isLast={false}
          onAddSwimlane={jest.fn()}
          onRemoveSwimlane={jest.fn()}
          useAnimation={false}
        />
      </DragDropContext>
    );
  });

  it("renders the main containers and text", () => {
    const title = swimlane.title;
    expect(screen.getByText(title)).toBeInTheDocument();
    expect(screen.getByTestId("swimlane")).toBeInTheDocument();
    expect(screen.getByTestId("swimlaneHead")).toBeInTheDocument();
    expect(screen.getByTestId("swimlaneBody")).toBeInTheDocument();
  });

  it("renders its stories", () => {
    const storiesExpected = stories.length;
    const storiesSeen = screen.queryAllByTestId("storyCard").length;
    expect(storiesExpected).toBe(storiesSeen);
  });
});

describe("it renders correctly in view mode when it has no stories", () => {
  let testData;
  let swimlane;
  let emptyStories;

  beforeEach(() => {
    testData = makeTestData(data);
    swimlane = testData.swimlane;
    emptyStories = testData.emptyStories;
    render(
      <DragDropContext onDragEnd={jest.fn()}>
        <Swimlane
          key={swimlane.id}
          id={swimlane.id}
          title={swimlane.title}
          stories={emptyStories}
          isEditMode={false}
          filterString={""}
          isFirst={false}
          isLast={false}
          onAddSwimlane={jest.fn()}
          onRemoveSwimlane={jest.fn()}
          useAnimation={false}
        />
      </DragDropContext>
    );
  });

  it("renders the main containers and text", () => {
    const title = swimlane.title;
    expect(screen.getByText(title)).toBeInTheDocument();
    expect(screen.getByTestId("swimlane")).toBeInTheDocument();
    expect(screen.getByTestId("swimlaneHead")).toBeInTheDocument();
    expect(screen.getByTestId("swimlaneBody")).toBeInTheDocument();
  });

  it("renders no stories", () => {
    const storiesSeen = screen.queryAllByTestId("storyCard").length;
    expect(storiesSeen).toBe(0);
  });
});

describe("it renders correctly in edit mode", () => {
  afterEach(() => {
    cleanup();
  });

  it("renders the main containers and text", () => {
    const { swimlane, stories } = makeTestData(data);
    const title = swimlane.title;
    render(
      <DragDropContext onDragEnd={jest.fn()}>
        <Swimlane
          key={swimlane.id}
          id={swimlane.id}
          title={swimlane.title}
          stories={stories}
          isEditMode={true}
          filterString={""}
          isFirst={false}
          isLast={false}
          onAddSwimlane={jest.fn()}
          onRemoveSwimlane={jest.fn()}
          useAnimation={false}
        />
      </DragDropContext>
    );

    // NB - the title is now in an input field, so look at
    // value rather than for the text itself.
    expect(screen.getByTestId("editableText").value).toBe(title);
    expect(screen.getByTestId("swimlane")).toBeInTheDocument();
    expect(screen.getByTestId("swimlaneHead")).toBeInTheDocument();
    expect(screen.getByTestId("swimlaneBody")).toBeInTheDocument();
  });

  it("renders its stories", () => {
    const { swimlane, stories } = makeTestData(data);
    render(
      <DragDropContext onDragEnd={jest.fn()}>
        <Swimlane
          key={swimlane.id}
          id={swimlane.id}
          title={swimlane.title}
          stories={stories}
          isEditMode={true}
          filterString={""}
          isFirst={false}
          isLast={false}
          onAddSwimlane={jest.fn()}
          onRemoveSwimlane={jest.fn()}
          useAnimation={false}
        />
      </DragDropContext>
    );
    const storiesExpected = stories.length;
    const storiesSeen = screen.queryAllByTestId("storyCard").length;
    // The expected number of stories are seen.
    expect(storiesExpected).toBe(storiesSeen);
  });

  it("displays expected edit controls when it has stories", () => {
    const { swimlane, stories } = makeTestData(data);
    render(
      <DragDropContext onDragEnd={jest.fn()}>
        <Swimlane
          key={swimlane.id}
          id={swimlane.id}
          title={swimlane.title}
          stories={stories}
          isEditMode={true}
          filterString={""}
          isFirst={false}
          isLast={false}
          onAddSwimlane={jest.fn()}
          onRemoveSwimlane={jest.fn()}
          useAnimation={false}
        />
      </DragDropContext>
    );
    // Delete icon should appear disabled, insert icon appears.
    expect(
      screen.getByTestId("deleteSwimlaneIconDisabled")
    ).toBeInTheDocument();
    expect(screen.getByTestId("insertSwimlane")).toBeInTheDocument();
  });

  it("displays expected edit controls when it has no stories", () => {
    const { swimlane, emptyStories } = makeTestData(data);
    render(
      <DragDropContext onDragEnd={jest.fn()}>
        <Swimlane
          key={swimlane.id}
          id={swimlane.id}
          title={swimlane.title}
          stories={emptyStories}
          isEditMode={true}
          filterString={""}
          isFirst={false}
          isLast={false}
          onAddSwimlane={jest.fn()}
          onRemoveSwimlane={jest.fn()}
          useAnimation={false}
        />
      </DragDropContext>
    );
    // the delete icon appears, and the insert swimlane icon appears.
    expect(screen.queryByTestId("deleteSwimlaneIcon")).toBeInTheDocument();
    expect(screen.getByTestId("insertSwimlane")).toBeInTheDocument();
  });

  it("displays expected edit controls when it is the first swimlane", () => {
    const { swimlane, emptyStories } = makeTestData(data);
    render(
      <DragDropContext onDragEnd={jest.fn()}>
        <Swimlane
          key={swimlane.id}
          id={swimlane.id}
          title={swimlane.title}
          stories={emptyStories}
          isEditMode={true}
          filterString={""}
          isFirst={true}
          isLast={false}
          onAddSwimlane={jest.fn()}
          onRemoveSwimlane={jest.fn()}
          useAnimation={false}
        />
      </DragDropContext>
    );
    // No delete icon should appear (neither enabled nor disabled). The insert icon should appear.
    expect(screen.queryByTestId("deleteSwimlane")).not.toBeInTheDocument();
    expect(
      screen.queryByTestId("deleteSwimlaneIconDisabled")
    ).not.toBeInTheDocument();
    expect(screen.getByTestId("insertSwimlane")).toBeInTheDocument();
  });

  it("displays expected edit controls when it is the last swimlane", () => {
    const { swimlane, emptyStories } = makeTestData(data);
    render(
      <DragDropContext onDragEnd={jest.fn()}>
        <Swimlane
          key={swimlane.id}
          id={swimlane.id}
          title={swimlane.title}
          stories={emptyStories}
          isEditMode={true}
          filterString={""}
          isFirst={false}
          isLast={true}
          onAddSwimlane={jest.fn()}
          onRemoveSwimlane={jest.fn()}
          useAnimation={false}
        />
      </DragDropContext>
    );
    // No delete icon should not appear (neither enabled nor disabled). The insert icon should not appear.
    expect(screen.queryByTestId("deleteSwimlane")).not.toBeInTheDocument();
    expect(
      screen.queryByTestId("deleteSwimlaneIconDisabled")
    ).not.toBeInTheDocument();
    expect(screen.queryByTestId("insertSwimlane")).not.toBeInTheDocument();
  });


});


describe("it handles interactivity correctly", () => {
  afterEach(() => {
    cleanup();
  });

  it("handles interactivity", () => {
    // NB - Swimlane doesn't handle drag-drop interactivity (that is 
    // done by Swimlanes as drag-drop can involve multiple swimlanes.
    // The only thing Swimlane handles is a title change.
    resetDispatcherMock();
    const { swimlane, emptyStories } = makeTestData(data);
    const newTitle = "New title";
    const {rerender} = render(
      <DragDropContext onDragEnd={jest.fn()}>
        <Swimlane
          key={swimlane.id}
          id={swimlane.id}
          title={swimlane.title}
          stories={emptyStories}
          isEditMode={true}
          filterString={""}
          isFirst={false}
          isLast={false}
          onAddSwimlane={jest.fn()}
          onRemoveSwimlane={jest.fn()}
          useAnimation={false}
        />
      </DragDropContext>
    );
    
    const field = screen.getByTestId("editableText");
    expect(swimlanesFired.type).toBe("notFired");
    fireEvent.change(field, { target: { value: newTitle } });
    // NB - the title will only dispatch once we come out of 
    // edit mode, so we have to rerender after firing change.
    rerender(
      <DragDropContext onDragEnd={jest.fn()}>
        <Swimlane
          key={swimlane.id}
          id={swimlane.id}
          title={swimlane.title}
          stories={emptyStories}
          isEditMode={false}
          filterString={""}
          isFirst={false}
          isLast={false}
          onAddSwimlane={jest.fn()}
          onRemoveSwimlane={jest.fn()}
          useAnimation={false}
        />
      </DragDropContext>
    );
    expect(swimlanesFired.type).toBe(EDIT_SWIMLANE);
    expect(swimlanesFired.data.change.title).toBe(newTitle);
  });
});

