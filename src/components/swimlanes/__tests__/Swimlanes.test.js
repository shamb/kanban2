import React from "react";
import { toBeInTheDocument } from "@testing-library/jest-dom";
import {
  getByText,
  render,
  screen,
  fireEvent,
  getAllByTestId, 
  within
} from "@testing-library/react";
import {
  mockGetComputedSpacing,
  mockDndElSpacing,
  makeDnd,
  DND_DRAGGABLE_DATA_ATTR,
  DND_DIRECTION_DOWN
} from "react-beautiful-dnd-test-utils";

import Swimlanes from "../Swimlanes";
import data from "../../../__dataForTests__/data";
import StoryVO from "../../../data/StoryVO";
import SwimlaneVO from "../../../data/SwimlaneVO";
import { MOVE_STORY } from "../../../reducers/storiesReducer";
import {
  ADD_SWIMLANE,
  REMOVE_SWIMLANE,
} from "../../../reducers/swimlanesReducer";

function renderAppForDragDrop() {
  const rtlUtils = render(
    <Swimlanes
      swimlanes={parsedData.swimlanes}
      stories={parsedData.stories}
      isEditMode={false}
      filterString=""
    />
  );
  mockDndElSpacing(rtlUtils);
  const makeGetDragEl = (text) => () =>
    rtlUtils.getByText(text).closest(DND_DRAGGABLE_DATA_ATTR);

  return { makeGetDragEl, ...rtlUtils };
};

jest.mock("../../../reducers/dispatchers", () => {
  return {
    setDispatchers: () => {},
    getDispatchers: () => {
      return {
        dispatchSwimlanes: (action) => {
          swimlanesFired = action;
        },
        dispatchStories: (action) => {
          storiesFired = action;
        },
      };
    },
  };
});
function resetDispatcherMock() {
  swimlanesFired = { type: "notFired" };
  storiesFired = { type: "notFired" };
}
let swimlanesFired;
let storiesFired;

// parsedData is the incoming data converted into VOs
const parsedData = {
  boardTitle: data.boardTitle,
  id: data.id,
  stories: data.stories.map((story) => new StoryVO(story)),
  swimlanes: data.swimlanes.map((swimlane) => new SwimlaneVO(swimlane)),
};

describe("it renders correctly during view mode", () => {
  beforeEach(() => {
    render(
      <Swimlanes
        swimlanes={parsedData.swimlanes}
        stories={parsedData.stories}
        isEditMode={false}
        filterString=""
      />
    );
  });

  it("outputs the main container on render", () => {
    expect(screen.getByTestId("swimlanes")).toBeInTheDocument();
  });

  it("renders the expected number of swimlanes", () => {
    expect(screen.getAllByTestId("swimlane").length).toBe(
      parsedData.swimlanes.length
    );
  });

});

describe("it renders swimlanes correctly during edit mode", () => {
  beforeEach(() => {
    render(
      <Swimlanes
        swimlanes={parsedData.swimlanes}
        stories={parsedData.stories}
        isEditMode={true}
        filterString=""
      />
    );
  });

  it("renders a delete button on all except the first and last swimlane", () => {
    const swimlanes = screen.getAllByTestId("swimlane");
    const firstSwimlane = swimlanes[0];
    const lastSwimlane = swimlanes[swimlanes.length-1];
    const innerSwimlanes = [...swimlanes].slice(1, -1);

    // The first and last swimlane are not deletable so should not have the deleteIcon.
    expect(within(firstSwimlane).queryByTestId("deleteSwimlaneIcon")).not.toBeInTheDocument();
    expect(within(lastSwimlane).queryByTestId("deleteSwimlaneIcon")).not.toBeInTheDocument();
    // The other swimlanes should have the deleteIcon (it will be enabled or disabled).
    innerSwimlanes.forEach(swimlane => {
      const hasDelete = within(swimlane).queryByTestId("deleteSwimlaneIcon") !== null
      const hasDeleteDisabled = within(swimlane).queryByTestId("deleteSwimlaneIconDisabled") !== null
      expect(hasDelete || hasDeleteDisabled).toBe(true);
    });

  })
})

describe("it handles interactivity correctly", () => {
  beforeEach(() => {
    resetDispatcherMock();
  });

  it("dispatches the expected event on a story drag-drop", async () => {
    mockGetComputedSpacing();
    const { getByText, makeGetDragEl } = renderAppForDragDrop();
    // Move the first story in the first swimlane down from index 0 to 1.
    await makeDnd({
      getByText,
      getDragEl: makeGetDragEl("Test story"),
      direction: DND_DIRECTION_DOWN,
      positions: 1,
    });

    // We expect an action to move the first story in test-file data
    // to be in swimlane 1, and for it to be moved down by 1.
    const from = storiesFired.data.from;
    const to = storiesFired.data.to;
    const swimlaneList = storiesFired.data.swimlaneList;

    expect(storiesFired.type).toBe(MOVE_STORY);
    expect(from.storyId).toBe(parsedData.stories[0].id);
    expect(from.storyIndex).toBe(0);
    expect(from.swimlaneId).toBe(parsedData.swimlanes[0].id);
    expect(to.storyId).toBe(parsedData.stories[0].id);
    expect(to.storyIndex).toBe(1);
    expect(to.swimlaneId).toBe(parsedData.swimlanes[0].id);
    swimlaneList.forEach((swimlaneId, index) => {
      expect(swimlaneId).toBe(parsedData.swimlanes[index].id);
    });
  });

  it("dispatches no event on a no-change drag-drop", async () => {
    mockGetComputedSpacing();
    const { getByText, makeGetDragEl } = renderAppForDragDrop();
    // Move the first story in the first swimlane down from index 0 to 0.
    // (i.e no change).
    await makeDnd({
      getByText,
      getDragEl: makeGetDragEl("Test story"),
      direction: DND_DIRECTION_DOWN,
      positions: 0,
    });

    expect(storiesFired.type).toBe("notFired");
  });

  it("dispatches the expected event on insert swimlane", () => {
    render(
      <Swimlanes
        swimlanes={parsedData.swimlanes}
        stories={parsedData.stories}
        isEditMode={true}
        filterString=""
      />
    );
    const insertButtons = screen.getAllByTestId("insertSwimlane");
    
    // Click on the first add swimlane button and show an event requesting 
    // a swimlane to be added after the first is seen.
    expect(swimlanesFired.type).toBe("notFired");
    fireEvent.click(insertButtons[0]);
    expect(swimlanesFired.type).toBe(ADD_SWIMLANE);
    expect(swimlanesFired.data.addAfterId).toBe(parsedData.swimlanes[0].id);
  });

  it("dispatched the expected event on delete swimlane", () => {
    jest.useFakeTimers();
    render(
      <Swimlanes
        swimlanes={parsedData.swimlanes}
        stories={parsedData.stories}
        isEditMode={true}
        filterString=""
      />
    );
    const deleteButtons = screen.getAllByTestId("deleteSwimlaneIcon");
    
    // Click on the first delete button and show an even requesting 
    // a swimlane to be deleted. Note - the delete is on a timer to 
    // allow the deletion animation to run through.
    expect(swimlanesFired.type).toBe("notFired");
    fireEvent.click(deleteButtons[0]);
    jest.runAllTimers();
    expect(swimlanesFired.type).toBe(REMOVE_SWIMLANE);
    // We can't know the returned id, so just check that it is a string of length > 0
    expect(swimlanesFired.data.id).toBeTruthy(); 
  });
});
