import React, { useState, memo, SyntheticEvent } from "react";
import PropTypes, { InferProps} from "prop-types";
// styling
//import styles from "./HeaderButtons.module.css"; >> Already imported in Header.

// Menu toggle-button, used in Header.

type ToggleButtonProps = InferProps<typeof ToggleButton.propTypes>

function ToggleButton(props:ToggleButtonProps) {
  const { id, classUnselected, classSelected, onChange, onClick } = props;
  const [toggled, setToggled] = useState(false);

  const btnClass:string = toggled ? classSelected : classUnselected;

  const clickHandler = (e:SyntheticEvent) => {
    const newState = !toggled;
    setToggled(newState);
    if (onChange) {
      onChange(newState);
    }
    // OnClick is largely redundant (onChange is better), but 
    // there just in case there is a need to get the click target.
    if (onClick) {
      onClick(e);
    }
  };

  return (
    <button id={id} data-testid={id} className={btnClass} onClick={clickHandler}>
      {props.children}
    </button>
  );
}

ToggleButton.propTypes = {
  id: PropTypes.string.isRequired,
  classUnselected: PropTypes.string.isRequired,
  classSelected: PropTypes.string.isRequired,
  onChange: PropTypes.func,
  onClick: PropTypes.func,
  children:PropTypes.string, // for ts
};

export default memo(ToggleButton);
