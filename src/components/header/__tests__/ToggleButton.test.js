import React from "react";
import { toBeInTheDocument } from "@testing-library/jest-dom";
import {
  render,
  screen,
  fireEvent,
} from "@testing-library/react";

import ToggleButton from "../ToggleButton";

function mockOnChange(toggleState) {
  changeReturn = toggleState;
}

function mockOnClick(e) {
  clickReturn = e;
}

function resetMocks() {
  changeReturn = null;
  clickReturn = null;
}

let changeReturn;
let clickReturn;

describe("it renders correctly", () => {
  const id = "someId";
  const classUnselected = "classUnselected";
  const classSelected = "classSelected";
  const buttonLabel = "toggle button";

  beforeEach(() => {
    resetMocks();
    render(
      <ToggleButton
        id={id}
        classUnselected={classUnselected}
        classSelected={classSelected}
        onChange={mockOnChange}
        onClick={mockOnClick}
      >
        {buttonLabel}
      </ToggleButton>
    );
  });

  it("renders with the correct class and label on start", () => {
    const instance = screen.getByTestId(id);

    expect(instance.className).toBe(classUnselected);
    expect(screen.getByText(buttonLabel)).toBeInTheDocument();
  });

  it("handles click and change correctly", () => {
    const instance = screen.getByTestId(id);

    //screen.debug();
    expect(changeReturn).toBe(null);
    expect(clickReturn).toBe(null);
    // Simulate a click and show the styling changes to classSelected, 
    // the change hander sees 'true, and the click hander sees an event. 
    fireEvent.click(instance);
    //screen.debug();
    expect(instance.className).toBe(classSelected);
    expect(changeReturn).toBe(true);
    expect(clickReturn).not.toBe(null);
    // Do another click and show the button toggles back to the unselected state.
    resetMocks();
    fireEvent.click(instance);
    expect(instance.className).toBe(classUnselected);
    expect(changeReturn).toBe(false);
    expect(clickReturn).not.toBe(null);
    //screen.debug()
  });
  
});
