import React from "react";
import Header from "../Header";
import { toBeInTheDocument } from "@testing-library/jest-dom";
import {
  render,
  screen,
  fireEvent,
} from "@testing-library/react";

import {HEADER_BUTTONS} from "../../../config/headerMenus";

// NB - we're mocking this so we can add an invalid button to it.
jest.mock("../../../config/headerMenus", () => {
  return {
    HEADER_BUTTONS: [
      {
        id: "reports",
        type: "button",
        label: "Reports",
        className: "btn icon reports"
      },
      {
        id: "editBoard",
        type:"toggle",
        label: "Edit Board",
        className: ["btn icon edit", "btn icon edit toggled"]
      },
      {
        id: null,
        type: null,
        label: null,
        className: null
      }
    ]
  };
});

jest.mock("../../../reducers/dispatchers", () => {
  return {
    setDispatchers: () => {},
    getDispatchers: () => {
      return {
        dispatchToasts: (action) => {
          toastFired = action;
        },
        dispatchStatus: (action) => {
          statusFired = action;
        },
        dispatchBoardTitle: (action) => {
          boardTitleFired = action;
        },
        setSearchTerm: (str) => {
          updatedSearchTerm = str;
        },
      };
    },
  };
});

function resetDispatcherMock() {
  toastFired = { type: "notFired" };
  statusFired = { type: "notFired" };
  boardTitleFired = { type: "notFired" };
  updatedSearchTerm= null;
}

let toastFired;
let statusFired;
let boardTitleFired;
let updatedSearchTerm;


describe("it renders all header components correctly on start", () => {
  const boardTitle = "Some board title";
  beforeEach(() => {
    render(<Header boardTitle={boardTitle} isEditMode={false} />);
  });

  it("renders the main container", () => {
    expect(screen.getByTestId("headerBar")).toBeInTheDocument();
  });

  it("renders the board title", () => {
    expect(screen.getByText(boardTitle)).toBeInTheDocument();
  });

  it("renders the search widget", () => {
    expect(screen.getByTestId("headerSearch")).toBeInTheDocument();
  });

  it("renders the button menu buttons", () => {
    HEADER_BUTTONS.forEach((buttonData) => {
      const label = buttonData.label || "unexpected";
      expect(screen.getByText(label)).toBeInTheDocument();
    });
  });

});

describe("it handles interaction correctly in view mode", () => {
  const boardTitle = "Some board title";

  beforeEach(() => {
    resetDispatcherMock();
    render(<Header boardTitle={boardTitle} isEditMode={false} />);
  });

  it("dispatches on search change", async () => {
    const searchClosed = screen.getByTestId("headerSearch");
    const searchTerm = "Search this";
    
    jest.useFakeTimers();
    expect(updatedSearchTerm).toBe(null);
    // Open the search field from its closed state.
    fireEvent.mouseOver(searchClosed);
    // Fire a change into the opened field (noting the code under 
    // test uses a timer, so run that to the end), and check the 
    // search value gets dispatched.
    fireEvent.change(screen.getByTestId("search"), {
      target: { value: searchTerm },
    });
    jest.runAllTimers();
    expect(updatedSearchTerm).toBe(searchTerm);
  });

  it("dispatches on reports button click", () => {
    const btn = screen.getByTestId("reports");

    expect(toastFired.type).toBe("notFired");
    fireEvent.click(btn);
    expect(toastFired).not.toBe("notFired");
  });

  it("dispatches on edit board button 'set' click", () => {
    const btn = screen.getByTestId("editBoard");

    expect(toastFired.type).toBe("notFired");
    expect(statusFired.type).toBe("notFired");
    fireEvent.click(btn);
    expect(toastFired).not.toBe("notFired");
    expect(toastFired.data.message).toBe("Entering edit mode...");
    expect(statusFired.type).not.toBe("notFired");
  });
});

describe("it handles interaction correctly in edit mode", () => {
  const boardTitle = "Some board title";

  beforeEach(() => {
    resetDispatcherMock();
  });

  it("dispatches on board title edit", () => {
    const { rerender } = render(
      <Header boardTitle={boardTitle} isEditMode={true} />
    );
    const field = screen.getByTestId("editableText");

    expect(boardTitleFired.type).toBe("notFired");
    // When the user changes the title, then exit edit mode, the title change should fire.
    fireEvent.change(field, { target: { value: "New Title" } });
    rerender(<Header boardTitle={boardTitle} isEditMode={false} />);
    expect(boardTitleFired.type).not.toBe("notFired");
  });

  it("dispatches on edit board button 'reset' click", () => {
    render(<Header boardTitle={boardTitle} isEditMode={true} />);
    const btn = screen.getByTestId("editBoard");

    expect(toastFired.type).toBe("notFired");
    expect(statusFired.type).toBe("notFired");
    fireEvent.click(btn);
    //screen.debug();
    //console.log("editFired", toastFired)
    expect(toastFired).not.toBe("notFired");
    expect(toastFired.data.message).toBe("Leaving edit mode...");
    expect(statusFired.type).not.toBe("notFired");
  });
});

