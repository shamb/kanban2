import React from "react";
import { toBeInTheDocument } from "@testing-library/jest-dom";
import {
  render,
  screen,
  fireEvent,
  waitFor,
} from "@testing-library/react";

import HeaderSearch from "../HeaderSearch";
import { act } from "react-dom/test-utils";
import HeaderSearchField from "../HeaderSearch";

function mockOnChange(str) {
  updatedSearchTerm = str;
}
function resetMocks() {
  updatedSearchTerm = null;
}

function simulateUserEnteringText(inputEl, str) {
  // NB -  there is a debounce delay, hence the need to
  // run all timers. This assumes the calling function has
  // already run jest.useFakeTimers().
  act(() => {
    fireEvent.mouseOver(inputEl);
    fireEvent.change(inputEl, { target: { value: str } });
    jest.runAllTimers();
  });
}

const closedStyle = "search";
const openStyle = "search open";
const userEnteredText = "A search term";
let updatedSearchTerm;

describe("it renders correctly", () => {
  const placeholder = "Search something";
  beforeEach(() => {
    resetMocks();
    // NB - we have to use act() for user events that can change state
    // See https://fb.me/react-wrap-tests-with-act
    act(() => {
      render(
        <div>
          <HeaderSearch onChange={mockOnChange} placeholder={placeholder} />
          <div
            id="clickableContainer"
            data-testid="clickableContainer"
            style={{ minHeight: "200px" }}
          ></div>
        </div>
      );
    });
  });

  /*Initial smoke test*/

  it("renders closed on first load", () => {
    const inputEl = screen.getByTestId("search");

    // Check for the outer container
    expect(screen.getByTestId("headerSearch")).toBeInTheDocument();
    // Check for the input element being in its closed position.
    expect(inputEl.className).toBe(closedStyle);
    // Check there is no placeholder.
    expect(inputEl.placeholder).toBe("");
    // Check there is a close icon.
    expect(screen.getByTestId("searchCloseIcon")).toBeInTheDocument();
  });

  /*Main functionality*/

  it("performs open/close functionality on mouse rollover/rollout", () => {
    const inputEl = screen.getByTestId("search");

    // NB - if we wanted to use real timers, we would remove jest.useFakeTimers()
    // and jest.runallTimers() and instead wrap the affected expects in
    // await waitfor(() => {...});. That would make the tests run for longer though,
    // plus it doesn't seem to change coverage (tried it)
    jest.useFakeTimers();

    // Check for the input element being in its closed position...
    expect(inputEl.className).toBe(closedStyle);
    // ... and in its open position on rollover.
    act(() => {
      fireEvent.mouseOver(inputEl);
    });
    expect(inputEl.className).toBe(openStyle);
    expect(inputEl.placeholder).toBe(placeholder);
    // ... and closes again when the user rolls out and the timer runs out.
    act(() => {
      fireEvent.mouseLeave(inputEl);
      jest.runAllTimers();
    });
    expect(inputEl.className).toBe(closedStyle);
    expect(inputEl.placeholder).toBe("");
  });

  it("captures the user's input, and stays open once populated", () => {
    const inputEl = screen.getByTestId("search");
    jest.useFakeTimers();

    expect(updatedSearchTerm).toBe(null);
    // Check for the input element being in its closed position...
    expect(inputEl.className).toBe(closedStyle);
    // Check the user text input is captured on change.
    simulateUserEnteringText(inputEl, userEnteredText);
    expect(updatedSearchTerm).toBe(userEnteredText);
    // confirm that the search stays open because it is not empty.
    act(() => {
      jest.runAllTimers();
    });
    expect(inputEl.className).toBe(openStyle);
  });

  it("checks input debounce is working", () => {
    const inputEl = screen.getByTestId("search");
    jest.useFakeTimers();

    // Simulate the user entering text and confirm no change has been sent.
    act(() => {
      fireEvent.mouseOver(inputEl);
      fireEvent.change(inputEl, { target: { value: "a" } });
      fireEvent.change(inputEl, { target: { value: "ab" } });
      fireEvent.change(inputEl, { target: { value: "abc" } });
    });
    expect(updatedSearchTerm).toBe(null);
    // Simulate adding more text and show the change is sent once the timer expires
    act(() => {
      fireEvent.change(inputEl, { target: { value: "abcd" } });
      jest.runAllTimers();
    });
    expect(updatedSearchTerm).toBe("abcd");
  });

  it("allows the user to clear their entry with the clear button, and the search auto-closes after a delay", async () => {
    const inputEl = screen.getByTestId("search");
    const clearBtn = screen.getByTestId("searchCloseIcon");
    jest.useFakeTimers();

    expect(inputEl.className).toBe(closedStyle);
    // Simulate the user entering text
    simulateUserEnteringText(inputEl, userEnteredText);
    expect(updatedSearchTerm).toBe(userEnteredText);
    expect(inputEl.className).toBe(openStyle);

    // Simulate the user clearing the text with the x button.
    // The field should clear immediately,then close after a delay.
    act(() => {
      fireEvent.click(clearBtn);
      fireEvent.mouseLeave(inputEl);
    });
    expect(updatedSearchTerm).toBe("");
    act(() => {
      jest.runAllTimers();
    });
    expect(inputEl.className).toBe(closedStyle);
  });

  it("shows if the user enters text and clicks outside of the searchbar, the searchbar stays open", () => {
    const inputEl = screen.getByTestId("search");
    const clickableContainer = screen.getByTestId("clickableContainer");
    jest.useFakeTimers();

    // confirm the field starts closed.
    expect(inputEl.className).toBe(closedStyle);
    // Simulate the user entering text
    simulateUserEnteringText(inputEl, userEnteredText);
    expect(updatedSearchTerm).toBe(userEnteredText);
    expect(inputEl.className).toBe(openStyle);

    // Simulate the user not clearing the text, and just leaving it to do other things,
    // and show the search stays open.
    act(() => {
      fireEvent.mouseLeave(inputEl);
      fireEvent.mouseEnter(clickableContainer);
      fireEvent.click(clickableContainer);
      jest.runAllTimers();
    });
    expect(inputEl.className).toBe(openStyle);
  });

  it("allows the user to clear search with the clear button, and immediately close the search with a click outside", async () => {
    const inputEl = screen.getByTestId("search");
    const clearBtn = screen.getByTestId("searchCloseIcon");
    const clickableContainer = screen.getByTestId("clickableContainer");
    jest.useFakeTimers();

    // confirm the field starts closed.
    expect(inputEl.className).toBe(closedStyle);
    // Simulate the user entering text
    simulateUserEnteringText(inputEl, userEnteredText);
    expect(updatedSearchTerm).toBe(userEnteredText);
    expect(inputEl.className).toBe(openStyle);

    // Simulate the user clearing the text with the x button, then clicking outside the search.
    act(() => {
      fireEvent.click(clearBtn);
      fireEvent.mouseLeave(inputEl);
      fireEvent.mouseEnter(clickableContainer);
      fireEvent.click(clickableContainer);
    });

    // NB - we can't really test whether this has closed on timeout or
    // blur event (because we need a waitFor for either), so I tested
    // via breakpoints, and its stopping on the blur event.
    await waitFor(() => {
      expect(inputEl.className).toBe(closedStyle);
    });
  });

  it("allows the user to clear their entry by entering '', and closing the search after a delay", async () => {
    const inputEl = screen.getByTestId("search");
    jest.useFakeTimers();

    // confirm the field starts closed.
    expect(inputEl.className).toBe(closedStyle);
    // Simulate the user entering text
    simulateUserEnteringText(inputEl, userEnteredText);
    expect(updatedSearchTerm).toBe(userEnteredText);
    expect(inputEl.className).toBe(openStyle);

    // Simulate the user entering "".
    simulateUserEnteringText(inputEl, "");
    expect(updatedSearchTerm).toBe("");
    expect(inputEl.className).toBe(openStyle);

    await waitFor(() => {
      expect(inputEl.className).toBe(openStyle);
    });
  });

  /* Edge cases */

  it("shows when the user pastes the same value into the field, no change event occurs", () => {
    const inputEl = screen.getByTestId("search");

    // Simulate the user entering text
    simulateUserEnteringText(inputEl, userEnteredText);
    expect(updatedSearchTerm).toBe(userEnteredText);
    expect(inputEl.className).toBe(openStyle);

    // clear the mock and show entering the same value does not
    // change the search term from mull (because no onChange is seen).
    resetMocks();
    act(() => {
      fireEvent.change(inputEl, { target: { value: userEnteredText } });
    })
    expect(updatedSearchTerm).toBe(null);
  });

  it("sends no change if the user clicks the x button on an empty field", () => {
    const inputEl = screen.getByTestId("search");
    const clearBtn = screen.getByTestId("searchCloseIcon");
    jest.useFakeTimers()

    act(() => {
      fireEvent.mouseOver(inputEl);
      fireEvent.click(clearBtn);
      jest.runAllTimers();
    });
    expect(updatedSearchTerm).toBe(null);

  });

  /*
  TODO: This test fails because relatedEvent is never valid, whereas it is in the browser. Fix or delete. 
  it("shows when the user rolls over the clear icon, it is not taken as a rollOut of the searchbar", () => {
    const inputEl = screen.getByTestId("search");
    const clearBtn = screen.getByTestId("searchCloseIcon");
    jest.useFakeTimers();

    // Simulate the user opening the search with rollover
    expect(inputEl.className).toBe(closedStyle);
    act(() => {
      fireEvent.mouseOver(inputEl);
    });
    expect(inputEl.className).toBe(openStyle);
    // Simulate the user rolling over the clear button and show the search 
    // stays open even though it is empty.
    act(() => {
      fireEvent.mouseLeave(inputEl); 
      fireEvent.mouseEnter(clearBtn)
      jest.runAllTimers();
    });
    expect(inputEl.className).toBe(openStyle);
  });
  */
  
});

describe("it shows defaults are set on no data", () => {
  beforeEach(() => {
    resetMocks();
  });

  it("uses placeholder default", () => {
    act(() => {
      render(<HeaderSearch onChange={mockOnChange} />);
    });
    const inputEl = screen.getByTestId("search");

    // Show the component has defaulted to the default placeholder
    act(() => {
      fireEvent.mouseEnter(inputEl);
    });
    expect(inputEl.placeholder).toBe(HeaderSearchField.PLACE_HOLDER);
  });

});
