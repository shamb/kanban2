// Framework libs
import React, { useEffect, useRef, memo, useCallback, SyntheticEvent, ReactNode, ReactElement } from "react";
import PropTypes, { InferProps } from "prop-types";
// Application - components
import ToggleButton from "./ToggleButton";
import HeaderSearchField from "./HeaderSearch";
import EditableField from "../common/editableField/EditableField";
// Application - data and data events
import { getDispatchers } from "../../reducers/dispatchers";
import { getNewAction } from "../../helpers/utils";
import { ADD_TOAST } from "../../reducers/toastReducer";
import { CHANGE_TITLE } from "../../reducers/boardTitleReducer";
import ToastVO from "../../data/ToastVO";
import { TOGGLE_EDIT_MODE } from "../../reducers/statusReducer";
// Application - config
import { HEADER_BUTTONS } from "../../config/headerMenus";
// interfaces and types
import { Action } from "../../helpers/TAction";
// Styling
import styles from "./Header.module.css";
import btnStyles from "./HeaderButtons.module.css";

// The Kanban header.

type HeaderProps = InferProps<typeof Header.propTypes>

function Header(props: HeaderProps): ReactElement {
  const { boardTitle, isEditMode } = props;
  const dispatchToasts = useRef<Function>();
  const dispatchStatus = useRef<Function>();
  const dispatchBoardTitle = useRef<Function>();
  const setSearchTerm = useRef<Function>();
  let buttons;

  const onChange = (changedTitle: string): void => {
    const action: Action = getNewAction(
      CHANGE_TITLE,
      { boardTitle: changedTitle },
      `user changed board title from ${boardTitle} to ${changedTitle}`
    );
    dispatchBoardTitle.current!(action);
  };

  useEffect(() => {
    // Get dispatchers on mount.
    const dispatchers = getDispatchers();
    dispatchToasts.current = dispatchers.dispatchToasts;
    dispatchStatus.current = dispatchers.dispatchStatus;
    dispatchBoardTitle.current = dispatchers.dispatchBoardTitle;
    setSearchTerm.current = dispatchers.setSearchTerm;
  }, []);

  const onClick = (e: SyntheticEvent): void => {
    const id = (e.target as HTMLButtonElement).id!;
    let data: any;
    let action: Action;

    switch (id) {
      case "editBoard":
        // Create toast.
        if (props.isEditMode) {
          data = {
            message: "Leaving edit mode...",
            type: ToastVO.INFO,
            duration: 1500,
            level: ToastVO.TOAST_LEVEL_DEBUG,
          };
        } else {
          data = {
            message: "Entering edit mode...",
            type: ToastVO.INFO,
            duration: 1500,
            level: ToastVO.TOAST_LEVEL_DEBUG,
          };
        }
        action = getNewAction(
          ADD_TOAST,
          data,
          "Adding edit mode change toast."
        );
        dispatchToasts.current!(action);
        // Toggle edit mode.
        action = getNewAction(TOGGLE_EDIT_MODE, {}, "Toggling edit mode.");
        dispatchStatus.current!(action);
        break;

      case "reports":
        data = {
          message: "Reports mode not currently available, watch this space!",
          type: ToastVO.WARNING,
          mustAccept: true,
          level: ToastVO.TOAST_LEVEL_TERSE,
        };
        action = getNewAction(ADD_TOAST, data, "Adding reports mode toast.");
        dispatchToasts.current!(action);
        break;

      default:
      // do nothing
    }
  };

  const onSearchChange = useCallback((newValue: string): void => {
    setSearchTerm.current!(newValue);
  }, []);

  buttons = HEADER_BUTTONS.map((button): ReactNode => {
    const { id, type, label, className } = button;
    let parsedClassName, parsedClassName2;
    switch (type) {
      case "button":
        parsedClassName = parseStyles(className);
        return (
          <button
            key={id}
            id={id}
            data-testid={id}
            type="button"
            className={parsedClassName}
            onClick={onClick}
          >
            {label}
          </button>
        );

      case "toggle":
        parsedClassName = parseStyles(className[0]);
        parsedClassName2 = parseStyles(className[1]);
        return (
          <ToggleButton
            key={id}
            id={id}
            classUnselected={parsedClassName}
            classSelected={parsedClassName2}
            onClick={onClick}
          >
            {label as any}
          </ToggleButton>
        );

      default:
        // Unexpected button or button type. Render a warning. This will
        // only work for the first unexpected button, but its really just
        // a helper to reveal a problem to dev.
        return (
          <span
            key={id || "unexpected"}
            data-testid={id || "unexpected"}>unexpected</span>
        );
    }
  });

  return (
    <header className={styles.headerBar} data-testid="headerBar">
      <h2 className="unselectable ellipses-text">
        <EditableField
          text={boardTitle}
          isEditMode={isEditMode}
          validationRegex="^([a-zA-Z0-9 ]){4,30}$"
          validationErrorText="Title must be between 4 and 30 alphanumeric characters"
          onChange={onChange}
        />
      </h2>
      <div className={styles.row}>
        <HeaderSearchField
          onChange={onSearchChange}
          placeholder="Search stories"
        />
        <div className={styles.row} data-testid="headerButtons">{buttons}</div>
      </div>
    </header>
  );
}

Header.propTypes = {
  boardTitle: PropTypes.string.isRequired,
  isEditMode: PropTypes.bool.isRequired,
};

/**Returns the styled module names given the raw style names.
 * e.g. 'btn icon edit' becomes
 * 'ToggleButton_btn__3k1Uv ToggleButton_icon__g4q_c ToggleButton_edit__3HWqV'
 */
function parseStyles(str: any) {
  return str
    .split(" ")
    .map((item: string) => btnStyles[item])
    .join(" ");
}

export default memo(Header);
