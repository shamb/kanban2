import React, { useState, useRef, useCallback, SyntheticEvent } from "react";
import PropTypes, { InferProps } from "prop-types";
// styles
import styles from "./HeaderSearch.module.css";

type HeaderSearchFieldProps = InferProps<typeof HeaderSearchField.propTypes>;

function HeaderSearchField(props:HeaderSearchFieldProps) {
  const {
    onChange,
    placeholder = HeaderSearchField.PLACE_HOLDER,
    debounceTime = HeaderSearchField.FILTER_DELAY,
    autoCloseTime = HeaderSearchField.AUTO_CLOSE_TIME,
  } = props;
  const [isOpen, setIsOpen] = useState(false);
  const inputRef = useRef<HTMLInputElement>(null);
  const value = useRef("");
  const debouncer = useRef<number>();
  const closeTimer = useRef<number>();
  let inputClass:string;
  let placeholderString:string;

  /**A search icon that expands to a search field on interaction,
   * and stays expanded as long as it contains a search value.
   */

  const onSearchFocus = useCallback(() => {
    // Set focus (as this does not occur for mouseover),
    // then open the search field.
    inputRef.current!.focus();
    clearInterval(closeTimer.current);
    setIsOpen(true);
  }, []);

  const onSearchBlur = useCallback((e) => {
    // If the search field is empty on losing focus, and
    // it did not lose focus to its own close button, close.
    const relatedTargetId = e.relatedTarget && e.relatedTarget.id;
    if (inputRef.current!.value === "") {
      clearInterval(closeTimer.current);
      if (relatedTargetId !== "searchCloseIcon") {
        setIsOpen(false);
      }
    }
  }, []);

  const onSearchMouseLeave = useCallback(
    (e) => {
    const timer:number = autoCloseTime as number;
      // If the user does not interact with an empty search bar
      // for too long, close it.
      if (e.relatedTarget.id !== "searchCloseIcon") {
        clearInterval(closeTimer.current);
        closeTimer.current = window.setTimeout(() => {
          if (value.current === "") {
            inputRef.current!.blur();
            setIsOpen(false);
          }
        }, timer);
      }
    },
    [closeTimer, autoCloseTime]
  );

  const onCloseClick = useCallback(
    (e:SyntheticEvent) => {
      // User clicked on the clear button.
      // Empty the search field, and send if changed.
      inputRef.current!.value = "";
      if (value.current !== "") {
        value.current = "";
        onChange("");
      }
    },
    [onChange]
  );

  const onInputChange = useCallback(() => {
    const timer:number = debounceTime as number;
    // Reset the timer, and send any change after debounce time
    // expiration.
    clearTimeout(debouncer.current);
    debouncer.current = window.setTimeout(() => {
      const domValue = inputRef.current!.value;
      if (domValue !== value.current) {
        value.current = domValue;
        onChange(domValue);
      }
    }, timer);
  }, [onChange, debounceTime]);

  if (!isOpen) {
    inputClass = styles.search;
    placeholderString = "";
  } else {
    inputClass = `${styles.search} ${styles.open}`;
    placeholderString = placeholder as string;
  }
  /**Note that we are using an <input type="text"> and not an <input type="search">.
   * We want to control the appearance of the close 'x' on the search because the
   * default is not consistent between browsers.
   */
  return (
    <div className={styles.searchContainer} data-testid="headerSearch">
      <input
        type="text"
        id="search"
        data-testid="search"
        className={inputClass}
        autoComplete="off"
        onMouseOver={onSearchFocus}
        onFocus={onSearchFocus}
        onBlur={onSearchBlur}
        onMouseLeave={onSearchMouseLeave}
        onChange={onInputChange}
        placeholder={placeholderString}
        ref={inputRef}
      />
      <button
        id="searchCloseIcon"
        data-testid="searchCloseIcon"
        className={styles.searchCloseIcon}
        onClick={onCloseClick}
      />
    </div>
  );
}

HeaderSearchField.FILTER_DELAY = 350;
HeaderSearchField.AUTO_CLOSE_TIME = 1200;
HeaderSearchField.PLACE_HOLDER = "Search";

HeaderSearchField.propTypes = {
  onChange: PropTypes.func.isRequired, // From parent: input change event. NB - use useCallback in the parent!
  placeholder: PropTypes.string, // From parent (optional): The field default text. Default is 'search'
  debounceTime: PropTypes.string, // From parent (optional). Change is sent only when no other change occurs within this time in ms.
  autoCloseTime: PropTypes.string, // From parent (optional). Time before an open, empty search bar is closed (ms).
};

export default HeaderSearchField;
