// Framework libs
import React, { ReactElement, useEffect, useRef } from "react";
import PropTypes, { InferProps } from "prop-types";
// Other libs
import { Flipped } from "react-flip-toolkit";
// Application
import { getDispatchers } from "../../reducers/dispatchers";
import { REMOVE_TOAST } from "../../reducers/toastReducer";
import { getNewAction } from "../../helpers/utils";
import { useTimeout } from "../../helpers/reactHelpers";
// interfaces and types
import { Action } from "../../helpers/TAction";
// styles
import styles from "./Toast.module.css";
import Nullable from "../../helpers/TNullable";

// Toast message. The toast can be dismissable or exist for a
// user defined timeout .

type ToastProps = InferProps<typeof Toast.propTypes>;

function Toast(props: ToastProps): ReactElement {
  const { id, message, type, duration, mustAccept = false } = props;
  const dispatchToasts = useRef<Function>();
  let toastClass: string;
  let theDuration: Nullable<number>;

  if (mustAccept) {
    toastClass = `${styles.toast} ${styles[type]} ${styles.mustAccept}`;
    theDuration = null;
  } else {
    toastClass = `${styles.toast} ${styles[type]}`;
    if (duration) {
      theDuration = duration;
    }
  }

  useEffect(() => {
    // Get dispatchers on mount.
    dispatchToasts.current = getDispatchers().dispatchToasts;
  }, []);

  const removeToast = () => {
    const action: Action = getNewAction(REMOVE_TOAST, { id }, `Remove toast ${id}.`);
    dispatchToasts.current!(action);
  };

  useTimeout(removeToast, theDuration);

  return (
    <Flipped flipId={id} spring="gentle" onAppear={onAppear} onExit={onExit}>
      <div className={toastClass} onClick={removeToast} data-testid="toast">
        <p>{message}</p>
      </div>
    </Flipped>
  );
}

Toast.propTypes = {
  id: PropTypes.string.isRequired,
  message: PropTypes.oneOfType([PropTypes.string, PropTypes.node]).isRequired,
  type: PropTypes.string.isRequired,
  duration: PropTypes.number,
  mustAccept: PropTypes.bool,
};

// transition functions for react-flip-toolkit.
// (we don't need to test these as they either won't ever run during 
//a test or testing these would be testing a third party lib).

/*istanbul ignore next*/
const onAppear = (el: HTMLElement, i: number) => {
  el.classList.add(styles.fadeToastIn);
  // Just in case the keyframe animation isn't accurate...
  setTimeout(() => {
    el.style.opacity = "1";
    el.classList.remove(styles.fadeToastIn);
  }, 310);
};

/*istanbul ignore next*/
const onExit = (el: HTMLElement, i: number, removeElement: Function) => {
  el.classList.add(styles.fadeToastOut);
  setTimeout(removeElement, 300);
};

export default Toast;
