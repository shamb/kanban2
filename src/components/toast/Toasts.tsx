// Framework libs
import React, { ReactElement, ReactNode } from "react";
import PropTypes from "prop-types";
// Other libs
import { Flipper } from "react-flip-toolkit";
// Application
import Toast from "./Toast";
import { getId } from "../../helpers/utils";
// interfaces and types
import IToastVO from "../../data/IToastVO";
// styling
import styles from "./Toasts.module.css";

// The list component containing all toasts.

//type ToastProps = InferProps<typeof Toasts.propTypes>
type ToastProps = {
  toasts:IToastVO[],
}

function Toasts(props:ToastProps):ReactElement {
  const { toasts } = props;
  const flipKey:string = getId();
  let toastList:ReactNode;

  toastList = toasts.map((toast:IToastVO):ReactNode => (
    <Toast
      key={toast.id}
      id={toast.id}
      message={toast.message}
      type={toast.type}
      mustAccept={toast.mustAccept}
      duration={toast.duration}
    />
  ));

  return (
    <Flipper className={styles.toasts} flipKey={flipKey}>
      <div className={styles.toastList} data-testid="toastList">
        {toastList}
      </div>
    </Flipper>
  );
}

Toasts.propTypes = {
  toasts: PropTypes.array.isRequired
};

export default Toasts;
