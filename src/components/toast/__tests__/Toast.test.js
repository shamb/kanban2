import React from "react";
import { toBeInTheDocument } from "@testing-library/jest-dom";
import {
  cleanup,
  fireEvent,
  getByTestId,
  getByText,
  render,
  screen,
} from "@testing-library/react";

import ToastVO from "../../../data/ToastVO";
import { REMOVE_TOAST } from "../../../reducers/toastReducer";
import Toast from "../Toast";

const data = {
  infoToast: {
    id: "infoToast",
    message: "Some message",
    type: ToastVO.INFO,
  },
  warningToast: {
    id: "warningToast",
    message: "Some message",
    type: ToastVO.WARNING,
  },
  htmlToast: {
    id: "htmlToast",
    message: (
      <>
        Some message in <strong data-testid="strong">Bold</strong>
      </>
    ),
    type: ToastVO.INFO,
  },
  mustAcceptToast: {
    id: "mustAcceptToast",
    message: "Some message",
    type: ToastVO.WARNING,
    mustAccept: true,
  },

  autoClosetToast: {
    id: "autoCloseToast",
    message: "Some message",
    type: ToastVO.WARNING,
    duration: 500,
  },

  closeToastOnUserClick: {
    id: "closeToastOnUserClick",
    message: "Some message",
    type: ToastVO.WARNING,
    mustAccept:true,
    duration: 500,
  },

  onlyRequiredProps: {
    id: "toastId",
    message: "Some message",
    type: ToastVO.INFO,
  },
};

jest.mock("../../../reducers/dispatchers", () => {
  return {
    setDispatchers: () => {},
    getDispatchers: () => {
      return {
        dispatchToasts: (action) => {
          toastFired = action;
        },
      };
    },
  };
});

function resetDispatcherMock() {
  toastFired = { type: "notFired" };
}

let toastFired;

describe("Toast renders the different toast types correctly", () => {
  it("renders an info toast", () => {
    render(<Toast {...data.infoToast} />);
    expect(screen.getByTestId("toast")).toBeInTheDocument();
    expect(screen.getByText(data.infoToast.message)).toBeInTheDocument();
    expect(screen.getByTestId("toast").className).toBe("toast info");
  });

  it("renders a warning toast", () => {
    render(<Toast {...data.warningToast} />);
    expect(screen.getByTestId("toast")).toBeInTheDocument();
    expect(screen.getByText(data.infoToast.message)).toBeInTheDocument();
    expect(screen.getByTestId("toast").className).toBe("toast warning");
  });

  it("renders a toast message that is JSX rather than a string", () => {
    render(<Toast {...data.htmlToast} />);
    expect(screen.getByTestId("toast")).toBeInTheDocument();
    expect(screen.getByTestId("strong")).toBeInTheDocument();
  });

  it("renders a must-accept toast correctly", () => {
    render(<Toast {...data.mustAcceptToast} />);
    // NB - classList is a DOMTokenList not Array, so we use .contains not .includes.
    expect(screen.getByTestId("toast").classList.contains("mustAccept")).toBe(
      true
    );
  });

  it("renders with minimum required props", () => {
    render(<Toast {...data.onlyRequiredProps} />);
    expect(screen.getByTestId("toast").classList.contains("info")).toBe(true);
    expect(
      screen.getByText(data.onlyRequiredProps.message)
    ).toBeInTheDocument();
  });
});

describe("Toast correctly appears and disappears", () => {
  beforeEach(() => {
    resetDispatcherMock();
  });
  afterEach(()=> {
    cleanup();
  });

  it("auto-closes on mustAccept=false", () => {
    jest.useFakeTimers();
    render(<Toast {...data.autoClosetToast} />);
    expect(toastFired.type).toBe("notFired");
    jest.runAllTimers();
    expect(toastFired.type).toBe(REMOVE_TOAST);
  });

  it("stays open until manually clicked for mustAccept=true", () => {
    jest.useFakeTimers();
    render(<Toast {...data.closeToastOnUserClick} />);
    expect(toastFired.type).toBe("notFired");
    jest.runAllTimers();
    expect(toastFired.type).toBe("notFired");
    fireEvent.click(screen.getByTestId("toast"));
    expect(toastFired.type).toBe(REMOVE_TOAST);
  });

  it("always disappears on user click for autoClose toasts", () => {
    jest.useFakeTimers();
    resetDispatcherMock();
    render(<Toast {...data.autoClosetToast} />);
    expect(toastFired.type).toBe("notFired");
    fireEvent.click(screen.getByTestId("toast"));
    expect(toastFired.type).toBe(REMOVE_TOAST);
  });

});
