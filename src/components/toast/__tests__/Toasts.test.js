import React from "react";
import { toBeInTheDocument } from "@testing-library/jest-dom";
import { cleanup, getByTestId, render, screen } from "@testing-library/react";

import ToastVO from "../../../data/ToastVO";
import Toasts from "../Toasts";

 const data = [
  {
    key:"key_1",
    id:"key_1",
    message:"A toast message",
    mustAccept:false,
    type:ToastVO.INFO,
  },
  {
    key:"key_2",
    id:"key_2",
    message:"A toast message",
    mustAccept:false,
    type:ToastVO.INFO,
  }
 ];

describe("Toasts renders correctly", () => {

  it("renders an empty list correctly", () => {
    render(<Toasts toasts={[]} />);
    expect(screen.getByTestId("toastList")).toBeInTheDocument();
    expect(screen.getByTestId("toastList").hasChildNodes()).toBe(false);
  });


  it("renders a populated list correctly", () => {
    render(<Toasts toasts={data} />);
    expect(screen.getByTestId("toastList")).toBeInTheDocument();
    expect(screen.getByTestId("toastList").childNodes.length).toBe(data.length);
  });
  
});