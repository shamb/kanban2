import React from "react";
import { toBeInTheDocument } from "@testing-library/jest-dom";
import { render, screen } from "@testing-library/react";

import Story from "../Story";

jest.mock("react-beautiful-dnd", () => ({
  Droppable: ({ children }) =>
    children(
      {
        draggableProps: {
          style: {},
        },
        innerRef: jest.fn(),
      },
      {}
    ),
  Draggable: ({ children }) =>
    children(
      {
        draggableProps: {
          style: {},
        },
        innerRef: jest.fn(),
      },
      {}
    ),
  DragDropContext: ({ children }) => children,
}));

describe("it renders correctly", () => {
  it("renders the unfiltered view", () => {
    const id = "someId";
    const index = 0;
    const title = "Some title.";
    const body = "Some body";

    const { rerender } = render(
      <Story
        id={id}
        index={index}
        isFiltered={true}
        title={title}
        body={body}
      />
    );
    expect(screen.getByTestId("storyCard")).toBeInTheDocument();
    expect(screen.getByText(title)).toBeInTheDocument();
    expect(screen.getByText(body)).toBeInTheDocument();

    rerender(
      <Story
        id={id}
        index={index}
        isFiltered={false}
        title={title}
        body={body}
      />
    );
    expect(screen.getByTestId("storyCardFiltered")).toBeInTheDocument();
    expect(screen.queryByText(title)).not.toBeInTheDocument();
    expect(screen.queryByText(body)).not.toBeInTheDocument();
  });
});
