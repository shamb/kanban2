// Framework libs
import React, { memo, ReactElement } from "react";
import PropTypes, { InferProps } from "prop-types";
import { Draggable } from "react-beautiful-dnd";
// Styling
import styles from "./Story.module.css";

// A draggable story summary.

type StoryProps = InferProps<typeof Story.propTypes>;

function Story(props: StoryProps): ReactElement {
  const { id, index, isFiltered, title, body } = props;

  if (isFiltered) {
    return (
      <Draggable draggableId={id} index={index}>
        {(provided) => (
          <div
            id={id}
            className={styles.storyCard}
            {...provided.draggableProps}
            {...provided.dragHandleProps}
            ref={provided.innerRef}
            data-testid="storyCard"
          >
            <h4>{title}</h4>
            {body}
          </div>
        )}
      </Draggable>
    );
  } else {
    return (
      <Draggable draggableId={id} index={index} isDragDisabled={true}>
        {(provided) => (
          <div
            id={id}
            className={styles.storyCardFiltered}
            {...provided.draggableProps}
            {...provided.dragHandleProps}
            ref={provided.innerRef}
            data-testid="storyCardFiltered"
          ></div>
        )}
      </Draggable>
    );
  }
}

Story.propTypes = {
  id: PropTypes.string.isRequired,
  index: PropTypes.number.isRequired,
  isFiltered: PropTypes.bool.isRequired,
  title: PropTypes.string.isRequired,
  body: PropTypes.string.isRequired,
};

export default memo(Story);
